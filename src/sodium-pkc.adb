with Interfaces.C ; use Interfaces.C ;

package body sodium.pkc is

  
   function crypto_box_seedbytes return size_t  -- ../include/sodium/crypto_box.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_seedbytes";

   function crypto_box_publickeybytes return size_t  -- ../include/sodium/crypto_box.h:29
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_publickeybytes";

   function crypto_box_secretkeybytes return size_t  -- ../include/sodium/crypto_box.h:33
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_secretkeybytes";

   function crypto_box_noncebytes return size_t  -- ../include/sodium/crypto_box.h:37
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_noncebytes";

   function crypto_box_macbytes return size_t  -- ../include/sodium/crypto_box.h:41
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_macbytes"; 
   
   function Nonce return Nonce_Ptr is
   begin
      return Nonce_Ptr(sodium.Create(crypto_box_noncebytes))  ;
   end Nonce ;
   
   function Seed( init : Unsigned_8 := 16#63# ) return Seed_Ptr is
      result : Seed_Ptr := Seed_Ptr(sodium.Create(crypto_box_seedbytes,init));
   begin
      result.all := (others => init) ;
      return result ;	
   end Seed ;


   function crypto_box_keypair (pk : Address ; 
                                sk : Address ) return int  -- ../include/sodium/crypto_box.h:57
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_keypair";
   

   function crypto_box_seed_keypair
     (pk : Address ;
      sk : Address ;
      seed : Address ) return int  -- ../include/sodium/crypto_box.h:52
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_seed_keypair";
   
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr ) is
      status : int ;
   begin
      pk := Public_KeyPtr(sodium.Create(crypto_box_publickeybytes));
      sk := Secret_KeyPtr(sodium.Create(crypto_box_secretkeybytes));
      status := crypto_box_keypair( pk.all'Address, sk.all'Address );
      if status /= 0
      then
         raise Program_Error with "crypto_box_keypair" ;
      end if ;
   end Generate ;
   
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr ; seed : Seed_Ptr ) is
      status : int ;
   begin
      pk := Public_KeyPtr(sodium.Create(crypto_box_publickeybytes));
      sk := Secret_KeyPtr(sodium.Create(crypto_box_secretkeybytes));
      status := crypto_box_seed_keypair( pk.all'Address , sk.all'Address , seed.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_box_seed_keypair" ;
      end if ;
   end Generate ;
   
   function crypto_scalarmult_base (pk : Address; sk : Address) return int  -- ../include/sodium/crypto_scalarmult.h:26
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_scalarmult_base";
   
   function Public( sk : Secret_KeyPtr ) return Public_KeyPtr is
      result : Public_KeyPtr := Public_KeyPtr(sodium.Create(crypto_box_publickeybytes) ) ;
      status : int ;
   begin
      status := crypto_scalarmult_base(result.all'Address , sk.all'Address) ;
      if status /= 0
      then
         raise Program_Error with "Public" ;
      end if ;
      return result ;
   end Public ;
   
   function crypto_box_easy
     (c : Address;
      m : Address;
      mlen : unsigned_long_long;
      n : Address;
      pk : Address;
      sk : Address) return int  -- ../include/sodium/crypto_box.h:61
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_easy";
   function Encrypt( m : Address ; mlen : Integer ;
                     n : Nonce_Ptr ;
                     rpk : Public_KeyPtr ;
                     ssk : Secret_KeyPtr ) return Block_Ptr is
      result : Block_Ptr := sodium.Create(size_t(mlen) + crypto_box_macbytes) ;
      status : int ;
   begin
      status := crypto_box_easy( result.all'Address , m , unsigned_long_long(mlen) ,
                                 n.all'Address , 
                                 rpk.all'Address ,
                                 ssk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_box_easy" ;
      end if ;
      return result ;
   end Encrypt ;
   
   function crypto_box_detached
     (c : Address;
      mac : Address;
      m : Address;
      mlen : unsigned_long_long;
      n : Address;
      pk : Address;
      sk : Address) return int  -- ../include/sodium/crypto_box.h:73
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_detached";
   
   procedure Encrypt( c : out Block_Ptr ;
                      mac : out MAC_Ptr ;
                      m : Address ; mlen : Integer ;
                      n : Nonce_Ptr ;
                      rpk : Public_KeyPtr ;
                      ssk : Secret_KeyPtr
                     ) is

      status : int ;
   begin
      c := Block_Ptr(sodium.Create(size_t(mlen))) ;
      mac := MAC_Ptr(sodium.Create(crypto_box_macbytes)) ;
      status := crypto_box_detached( c.all'Address , mac.all'Address ,
                                     m , unsigned_long_long(mlen) ,
                                 n.all'Address , 
                                 rpk.all'Address ,
                                 ssk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_box_detached" ;
      end if ;
   end Encrypt ;
   
   function crypto_box_open_easy
     (m : Address;
      c : Address;
      clen : unsigned_long_long;
      n : Address;
      pk : Address;
      sk : Address) return int  -- ../include/sodium/crypto_box.h:67
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_open_easy";
   
   function Decrypt( c : Address ; clen : Integer ;
                     n : Nonce_Ptr ;
                     spk : Public_KeyPtr ;
                     rsk : Secret_KeyPtr ) return Block_Ptr is
      result : Block_Ptr := sodium.Create ( size_t(clen) - crypto_box_macbytes );
      status : int ;
   begin
      status := crypto_box_open_easy(result.all'address ,
                                     c , unsigned_long_long(clen) , 
                                     n.all'Address , 
                                     spk.all'Address , rsk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_box_open_easy" ;
      end if ;
      return result ;
   end Decrypt ;
   function crypto_box_open_detached
     (m : Address;
      c : Address;
      mac : Address;
      clen : unsigned_long_long;
      n : Address;
      pk : Address;
      sk : Address) return int  -- ../include/sodium/crypto_box.h:80
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_open_detached";
   procedure Decrypt( m : out Block_Ptr ;
                      c : Address ; clen : Integer ;
                      mac : MAC_Ptr ;
                      n : Nonce_Ptr ;
                      spk : Public_KeyPtr ;
                      rsk : Secret_KeyPtr ) is
      status : int ;
   begin
      m := sodium.Create( size_t(clen)  ) ;
      status := crypto_box_open_detached( m.all'Address ,
                                          c , mac.all'Address , unsigned_long_long(clen) ,
                                          n.all'Address ,
                                          spk.all'Address ,
                                          rsk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_box_open_detached" ;
      end if ;                                    
   end Decrypt ;
   
   function crypto_box_beforenmbytes return size_t  -- ../include/sodium/crypto_box.h:92
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_beforenmbytes";

   function crypto_box_beforenm
     (k : Address ;
      pk : Address ;
      sk : Address ) return int  -- ../include/sodium/crypto_box.h:95
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_beforenm";
   
 
   function PreCalc( pk : Public_KeyPtr ; sk : Secret_KeyPtr ) return Shared_KeyPtr is
      result : Shared_KeyPtr ;
      status : int ;
   begin
      result := Shared_KeyPtr(sodium.Create(crypto_box_beforenmbytes) ) ;
      status := crypto_box_beforenm(result.all'Address , pk.all'Address , sk.all'Address) ;
      if status /= 0
      then
         raise Program_Error with "crypto_box_beforenm" ;
      end if ;
      return result ;
   end PreCalc ;
   function crypto_box_easy_afternm
     (c : Address ;
      m : Address ;
      mlen : unsigned_long_long;
      n : Address ;
      k : Address ) return int  -- ../include/sodium/crypto_box.h:100
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_easy_afternm";

   function crypto_box_open_easy_afternm
     (m : Address ;
      c : Address ;
      clen : unsigned_long_long;
      n : Address ;
      k : Address ) return int  -- ../include/sodium/crypto_box.h:105
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_open_easy_afternm";

   
   function Encrypt( m : Address ; mlen : Integer ;
                     n : Nonce_Ptr ;
                     shk : Shared_KeyPtr ) return Block_Ptr is
      result : Block_Ptr := sodium.Create( size_t(mlen) + crypto_box_macbytes) ;
      status : int ;
   begin
      status := crypto_box_easy_afternm( result.all'Address , m , unsigned_long_long(mlen) ,
                                         n.all'Address , 
                                         shk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_box_easy_afternm" ;
      end if ;
      return result ;
   end Encrypt ;
   
   function Decrypt( c : Address ; clen : Integer ;
                     n : Nonce_Ptr ;
                     shk : Shared_KeyPtr ) return Block_Ptr is
      result : Block_Ptr := sodium.Create( size_t(clen) - crypto_box_macbytes);
      status : int ;
   begin
      status := crypto_box_open_easy_afternm(result.all'address ,
                                     c , unsigned_long_long(clen) , 
                                     n.all'Address , 
                                     shk.all'Address ); 
      if status /= 0
      then
         raise Program_Error with "crypto_box_open_easy_afternm" ;
      end if ;
      return result ;
   end Decrypt ;
     function crypto_box_detached_afternm
     (c : Address;
      mac : Address;
      m : Address;
      mlen : unsigned_long_long;
      n : Address;
      k : Address) return int  -- ../include/sodium/crypto_box.h:111
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_detached_afternm";

   function crypto_box_open_detached_afternm
     (m : Address;
      c : Address;
      mac : Address;
      clen : unsigned_long_long;
      n : Address;
      k : Address) return int  -- ../include/sodium/crypto_box.h:117
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_open_detached_afternm";
   
   procedure Encrypt( c : out Block_Ptr ;
                      mac : out MAC_Ptr ;
                     m : Address ; mlen : Integer ;
                     n : Nonce_Ptr ;
                      shk : Shared_KeyPtr ) is
      status : int ;
   begin
      c := Block_Ptr(sodium.Create(size_t(mlen))) ;
      mac := MAC_Ptr(sodium.Create(crypto_box_macbytes)) ;
      status := crypto_box_detached_afternm( c.all'Address , mac.all'Address ,
                                     m , unsigned_long_long(mlen) ,
                                 n.all'Address , 
                                 shk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_box_detached_afternm" ;
      end if ;
   end Encrypt ;
   
   function Decrypt( c : Address ; 
                     mac : MAC_Ptr ;
                     clen : Integer ;
                     n : Nonce_Ptr ;
                     shk : Shared_KeyPtr ) return Block_Ptr is
      status : int ;
      result : Block_Ptr := sodium.Create( size_t(clen) );
   begin
      status := crypto_box_open_detached_afternm( result.all'Address ,
                                          c , mac.all'Address , unsigned_long_long(clen) ,
                                          n.all'Address ,
                                          shk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_box_open_detached_afternm" ;
      end if ;     
      return result ;  
   end Decrypt ;
   
end sodium.pkc;
