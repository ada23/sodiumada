------------------------------------------------------------------------------
-- https://doc.libsodium.org/
------------------------------------------------------------------------------

with Interfaces ;
with Interfaces.C ; use Interfaces.C ;
with Ada.Streams ;
package sodium is

   type Block_Type is array (natural range <>) of Interfaces.Unsigned_8 ;
   type Block_Ptr is  access all Block_Type ;

   function Initialize return boolean ;
   function Value( b64str : String ) return Block_Ptr ;
   function Image( key : Block_Ptr ) return String ;
   function Create( length : size_t ;
                    init : Interfaces.Unsigned_8 := 16#89# ) return Block_Ptr ;
   procedure Destroy( block : in out Block_Ptr );


end sodium;
