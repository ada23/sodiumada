with System ; use System ;
with Interfaces.C; use interfaces.C ;

with Text_Io; use Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO ;

package body sodium.pksb is
   
  function crypto_box_seedbytes return size_t  -- ../include/sodium/crypto_box.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_seedbytes";

   function crypto_box_publickeybytes return size_t  -- ../include/sodium/crypto_box.h:29
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_publickeybytes";

   function crypto_box_secretkeybytes return size_t  -- ../include/sodium/crypto_box.h:33
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_secretkeybytes";

   function crypto_box_noncebytes return size_t  -- ../include/sodium/crypto_box.h:37
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_noncebytes";

   function crypto_box_macbytes return size_t  -- ../include/sodium/crypto_box.h:41
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_box_macbytes";
       
   function crypto_box_keypair (pk : Address ; 
                                sk : Address ) return int  -- ../include/sodium/crypto_box.h:57
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_keypair";
   
   SEALBYTES : Integer ;

   procedure Generate( pk : out Public_KeyPtr ;
                       sk : out Secret_Keyptr ) is
      status : int ;
   begin
      pk := Public_KeyPtr(sodium.Create(crypto_box_publickeybytes))  ;
      sk := Secret_KeyPtr(sodium.Create(crypto_box_secretkeybytes)) ;
      status := crypto_box_keypair( pk.all'Address , sk.all'Address ) ;
   end Generate ;
   
   
   function crypto_box_seal
     (c : System.Address ;
      m : System.Address ;
      mlen : unsigned_long_long;
      pk : Address ) return int  -- ../include/sodium/crypto_box.h:130
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_seal";

   function Seal( ctp : System.Address ;
                  ctl : Integer ;
                  pk : Public_KeyPtr ) return EncryptedBlock_Ptr is
      result : EncryptedBlock_Ptr ;
      mlen : unsigned_long_long :=
        unsigned_long_long( ctl ) ;
      status : int ;
      -- SEALBYTES : integer := integer(crypto_box_publickeybytes + crypto_box_macbytes)  ;
   begin
      result := new EncryptedBlock_Type(1.. SEALBYTES + ctl ) ;
      status := crypto_box_seal( result.all'Address , 
                                 ctp ,
                                 mlen ,
                                 pk.all'Address );
      if status /= 0
      then
         Put("Failed to encrypt cleartext of "); Put(ctl); Put_Line(" bytes");
         raise Program_Error with "Seal" ;
      end if ;
      
      return result ;
   end Seal ;
   
    function crypto_box_seal_open
     (m : System.Address ;
      c : System.Address ;
      clen : unsigned_long_long;
      pk : Address ;
      sk : Address ) return int  -- ../include/sodium/crypto_box.h:135
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_box_seal_open";
   
   function UnSeal( etp : System.Address ;
                    etl : Integer ;
                    pk : Public_KeyPtr ;
                    sk : Secret_KeyPtr ) return ClearText_Ptr is
      status : int ;
      result : ClearText_Ptr ;
      emlen : unsigned_long_long := 
        unsigned_long_long( etl) ;
   begin 
      result := new ClearText_Type(1..natural(emlen) - SEALBYTES );
      status := crypto_box_seal_open( result.all'address ,
                                      etp ,
                                      emlen ,
                                      pk.all'Address ,
                                      sk.all'Address ) ;
      if status /= 0
      then
         Put("Unable to decrypt message of "); Put(integer(emlen)); Put_Line(" bytes");
         raise Program_Error with "UnSeal" ;
      end if ;
      
      return result ;
   end UnSeal ;
   
begin
   SEALBYTES := integer(crypto_box_publickeybytes + crypto_box_macbytes)  ;
end sodium.pksb ;
