package body sodium.random is
  function randombytes_seedbytes return size_t  -- ../include/sodium/randombytes.h:32
   with Import => True,
        Convention => C,
     External_Name => "randombytes_seedbytes";
   function Seed (fill : Unsigned_8 := 16#1f# ) return Seed_Ptr is
      result : Seed_Ptr ;
   begin
      result := Seed_Ptr( sodium.Create(randombytes_seedbytes) ) ;
      result.all := (others => fill) ;
      return result ;
   end Seed ;

end sodium.random ;
