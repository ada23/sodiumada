------------------------------------------------------------------------------
-- Key derivation
-- https://doc.libsodium.org/key_derivation
------------------------------------------------------------------------------
with System; use System;
with Interfaces; use Interfaces ;
with Unchecked_Deallocation ;

package sodium.pwdhash.kdf is
   
   type MasterKeyPtr_Type is new Block_Ptr ;
   type SubKeyPtr_Type is new Block_Ptr ;
   
   -- procedure Free is new Unchecked_Deallocation(Block_Type,SubkeyPtr_Type);
   function Create( generate : boolean := false ) return MasterKeyPtr_Type ;
   function DeriveFromKey
     (subkey_id : Unsigned_64 ; ctx : String ; key : MasterKeyPtr_Type ) return SubKeyPtr_Type ;
   procedure DeriveFromKey
     ( subkey : Address ;
       subkey_len : integer ;
       ctx : String ; key : MasterKeyPtr_Type ;subkey_id : Unsigned_64) ;    
end sodium.pwdhash.kdf;
