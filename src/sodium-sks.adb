with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions; use Interfaces.C.Extensions ;
with Ada.Text_io; use Ada.Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;
with Ada.Streams; use Ada.Streams;
with Ada.Streams.Stream_Io; use Ada.Streams.Stream_Io ;

with GNAT.Source_Info ;

with sodium.pwdhash ;
with sodium.helpers ;

package body sodium.sks is

   function crypto_secretstream_xchacha20poly1305_keybytes return size_t  -- /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:30
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretstream_xchacha20poly1305_keybytes";

   procedure crypto_secretstream_xchacha20poly1305_keygen (k : Address )  -- /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:66
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretstream_xchacha20poly1305_keygen";


  function crypto_secretstream_xchacha20poly1305_statebytes return size_t  -- /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:63
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretstream_xchacha20poly1305_statebytes";

  function crypto_secretstream_xchacha20poly1305_headerbytes return size_t  -- /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretstream_xchacha20poly1305_headerbytes";

   function crypto_secretstream_xchacha20poly1305_abytes return size_t  -- /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:20
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretstream_xchacha20poly1305_abytes";

   crypto_secretstream_xchacha20poly1305_TAG_MESSAGE : constant := 16#00#;  --  /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:38

   crypto_secretstream_xchacha20poly1305_TAG_PUSH : constant := 16#01#;  --  /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:42

   crypto_secretstream_xchacha20poly1305_TAG_REKEY : constant := 16#02#;  --  /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:46
   crypto_secretstream_xchacha20poly1305_TAG_FINAL : constant := 
         crypto_secretstream_xchacha20poly1305_TAG_PUSH + crypto_secretstream_xchacha20poly1305_TAG_REKEY ;

  function crypto_secretstream_xchacha20poly1305_init_push
     (state : Address ;
      header : Address ;
      k : Address ) return int  -- /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:71
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretstream_xchacha20poly1305_init_push";

   function crypto_secretstream_xchacha20poly1305_push
     (state : Address ;
      c : Address ;
      clen_p : access Interfaces.C.Extensions.unsigned_long_long;
      m : Address ;
      mlen : Interfaces.C.Extensions.unsigned_long_long;
      ad : Address := System.Null_Address ;
      adlen : Interfaces.C.Extensions.unsigned_long_long := 0 ;
      tag : Interfaces.C.unsigned_char ) return int  -- /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:78
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretstream_xchacha20poly1305_push";

 function crypto_secretstream_xchacha20poly1305_init_pull
     (state : System.Address ;
      header : System.Address;
      k : System.Address ) return int  -- /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:86
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretstream_xchacha20poly1305_init_pull";

   function crypto_secretstream_xchacha20poly1305_pull
     (state : Address ;
      m : Address ;
      mlen_p : access Extensions.unsigned_long_long;
      tag_p : access Interfaces.C.unsigned_char ;
      c : Address ;
      clen : Extensions.unsigned_long_long;
      ad : Address := System.Null_Address;
      adlen : access Extensions.unsigned_long_long := null ) return int  -- /Users/rajasrinivasan/include/sodium/crypto_secretstream_xchacha20poly1305.h:93
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretstream_xchacha20poly1305_pull";
        
    function Generate return KeyPtr_Type is 
       result : KeyPtr_Type ;
    begin
       result := KeyPtr_Type( sodium.Create(crypto_secretstream_xchacha20poly1305_keybytes) ) ;
       crypto_secretstream_xchacha20poly1305_keygen( result.all'Address ) ;
       return result ;
    end Generate ;

    function Generate( pwd : String) return KeyPtr_Type is
       result : KeyPtr_Type ;
       salt : sodium.pwdhash.SaltPtr_type := sodium.pwdhash.Create (true) ;
       final : sodium.pwdhash.KeyPtr_Type ;
    begin
       salt.all := (Others => 16#77#) ;
       result := KeyPtr_Type( sodium.Create(crypto_secretstream_xchacha20poly1305_keybytes) ) ;
       final := sodium.pwdhash.DeriveKey(pwd,salt,sodium.pwdhash.MODERATE,result.all'Length) ;
       result.all := final.all ;
       --Put("Expected "); Put(result.all'Length); Put(" got "); Put(final.all'Length); New_Line;
       return result ;
    end Generate ;

    function Generate( pwd : String ; salt : in out sodium.pwdhash.SaltPtr_type) return KeyPtr_Type is
       result : KeyPtr_Type ;
       final : sodium.pwdhash.KeyPtr_Type ;
    begin
       result := KeyPtr_Type( sodium.Create(crypto_secretstream_xchacha20poly1305_keybytes) ) ;
       final := sodium.pwdhash.DeriveKey(pwd,salt,sodium.pwdhash.MODERATE,result.all'Length) ;
       result.all := final.all ;
       --Put("Expected "); Put(result.all'Length); Put(" got "); Put(final.all'Length); New_Line;
       return result ;
    end Generate ;

    procedure Save(key : KeyPtr_Type; filename: String) is
        keyfile : Ada.Text_Io.File_Type ;
        keystr : string := sodium.helpers.bin2base64(key.all'Address,key.all'Length) ;
    begin
       Ada.Text_Io.Create(keyfile,Ada.Text_io.Out_File,filename);
       Ada.Text_Io.Put_Line(keyfile,keystr);
       Ada.Text_Io.Close(keyfile);
    end Save ;

    procedure Load(filename: String ; key : out KeyPtr_Type) is
       keyfile : Ada.Text_Io.File_Type ;
       keystr : string( 1 .. 4*Integer(crypto_secretstream_xchacha20poly1305_keybytes)) ;
       keystrlen : natural ;
    begin
       Ada.Text_Io.Open(keyfile,Ada.Text_Io.In_File,filename);
       Ada.Text_Io.Get_Line(keyfile,keystr,keystrlen);
       Ada.Text_Io.Close(keyfile);
       key := KeyPtr_Type( sodium.helpers.base642bin(keystr(1..keystrlen))) ;
    end Load ;

    function Create return State_Type is
       result : State_Type ;
    begin
       result := State_Type(sodium.Create( crypto_secretstream_xchacha20poly1305_statebytes )) ;
       return result ;
    end Create ;

    function Create return HeaderPtr_Type is
        result : HeaderPtr_Type ;
    begin
        result := HeaderPtr_Type(sodium.Create( crypto_secretstream_xchacha20poly1305_headerbytes )) ;
        return result ;
    end Create ;

    procedure NL is
    begin
       New_Line;
    end NL ;
    procedure Field(t : String; col : Ada.Text_Io.Count) is
    begin
       Put(t); Set_Col(col);
    end Field ;

    procedure Create( str : out Session_Type ; pwd : String ; hdr : in out HeaderPtr_Type ; 
                      mode : Mode_Type ) is
       --result : State_Type := Create ;
       key : KeyPtr_Type := Generate (pwd) ;
       status : int ;
    begin
       Put("Password Key : ");
       Put_Line(sodium.helpers.bin2hex(key.all'Address,key.all'Length));
       str := Session_Type(sodium.Create( crypto_secretstream_xchacha20poly1305_statebytes)) ;
       if mode = Out_Stream
       then
         hdr := Create ;
         status := crypto_secretstream_xchacha20poly1305_init_push( str.all'Address , hdr.all'Address , key.all'Address) ;
       else
         status := crypto_secretstream_xchacha20poly1305_init_pull( str.all'Address , hdr.all'Address , key.all'Address) ;
       end if ;      
      if status /= 0
      then
         raise Program_Error with GNAT.Source_Info.Source_Location ;
      end if ;
 
      --str := Session_Type(result) ;
    end Create ;

    procedure Read( str : in out Session_Type ; blockptr : Address; blocklen : Integer ; outbytes : out Block_Ptr ; last : out boolean ) is
      status : int ;
      tag : aliased Interfaces.C.unsigned_char ;
      ctext : sodium.Block_Ptr := sodium.Create( size_t(blocklen) - crypto_secretstream_xchacha20poly1305_abytes);
    begin
      Put("Attempting to read "); Put(blocklen); Put(" bytes"); New_Line;
      Put_Line(sodium.helpers.bin2hex(blockptr,blocklen)) ;
      status := crypto_secretstream_xchacha20poly1305_pull(str.all'Address , ctext.all'Address , null , tag'Access , 
                               blockptr , Interfaces.C.Extensions.unsigned_long_long(blocklen) );
      if status /= 0
      then
         raise Program_Error with GNAT.Source_Info.Source_Location ;
      end if ;
      Put("Decrypted Text "); sodium.helpers.ShowHex(ctext);
      if tag = crypto_secretstream_xchacha20poly1305_TAG_FINAL 
      then
         last := true ; 
      else
         last := false;
      end if ;
      outbytes := ctext ;
    end Read ;

    procedure Write(str : in out Session_Type ; blockptr : Address; blocklen : Integer ; outbytes : out Block_Ptr; last : boolean := false ) is
      tag : unsigned_char := 0 ;
      status : int ;
      ciphertext : Block_Ptr ;
    begin
      Put("Encrypting : ");
      Put_Line(sodium.helpers.bin2hex(blockptr , blocklen)) ;
      if last
      then
         tag := crypto_secretstream_xchacha20poly1305_TAG_FINAL ;
      end if ;
      ciphertext := sodium.Create( size_t(blocklen) + crypto_secretstream_xchacha20poly1305_abytes) ;
      status := crypto_secretstream_xchacha20poly1305_push( str.all'Address , ciphertext.all'Address, null , 
                       blockptr , Interfaces.C.Extensions.unsigned_long_long(blocklen) , System.Null_Address , 0 , tag );
      if status /= 0
      then
         raise Program_Error with GNAT.Source_Info.Source_Location ;
      end if ;
      outbytes := ciphertext ;
      Put_Line("Encrypted text");
      Put_Line(sodium.helpers.bin2hex(ciphertext.all'Address , ciphertext.all'Length)) ;
    end Write ;

    procedure Close(str : in out Session_Type ) is
    begin
       sodium.Destroy (Block_Ptr(str)) ;
    end Close ;

   ChunkLength : constant := 10_000 ;
   procedure Read( str : in out Ada.Streams.Stream_IO.File_Type ; blk : out Block_Ptr ; wlen : Short_Integer := 0 ) is
      use Ada.Streams ;
      blklen : aliased Short_Integer := wlen ;
   begin
      if wlen = 0
      then
         Short_Integer'Read(Ada.Streams.Stream_Io.Stream(str) , blklen) ;
      end if ;
      declare
         blklenbytes : Stream_Element_Array(1..Stream_Element_Offset(blklen)) ;
         blkbytesread : Ada.Streams.Stream_Element_Offset ;
      begin
         Ada.Streams.Stream_Io.Read( str , blklenbytes , blkbytesread );
         blk := sodium.helpers.Create(blklenbytes(1..blkbytesread)) ;
      end ;
   end Read ;
   procedure Write( str : in out Ada.Streams.Stream_IO.File_Type ; blk : Block_Ptr ; wlen : boolean ) is
      use Ada.Streams.Stream_Io ;
      blklen : aliased Short_Integer := blk.all'Length ;
   begin
      if wlen
      then
         Short_Integer'Write( Stream(str) , blklen );
      end if ;
      declare
         blklenbytes : Stream_Element_Array(1..Stream_Element_Offset(blk.all'length)) ;
         for blklenbytes'Address use blk.all'Address ;
      begin
         Ada.Streams.Stream_Io.Write( str , blklenbytes);
      end ;
   end Write ;


   procedure Encrypt( filein : string ; fileout : string ; pwd : string ) is
       inpfile : Ada.Streams.Stream_Io.File_Type ;
       inpchunk : Ada.Streams.Stream_Element_Array(1..ChunkLength);
       inpchunklen : Ada.Streams.Stream_Element_Offset ;

       hdr : HeaderPtr_Type ;
       encstream : Session_Type ;
       encblk : Block_Ptr ;

      outfile : Ada.Streams.Stream_Io.File_Type ;
 
      
   begin
       Create(encstream, pwd , hdr, Out_Stream ) ; 
       Ada.Streams.Stream_Io.Open(inpfile,Ada.Streams.Stream_Io.In_File,filein) ;
       Ada.Streams.Stream_IO.Create(outfile,Ada.Streams.Stream_IO.Out_File,fileout) ;
       Write(outfile,Block_Ptr(hdr),false);
       Put("Encrypt Header : ");
       Put_Line(sodium.helpers.bin2hex(hdr.all'Address, hdr.all'Length)) ;

       while not Ada.Streams.Stream_Io.End_Of_File(inpfile)
       loop
         Ada.Streams.Stream_Io.Read(inpfile,inpchunk,inpchunklen) ;
         Write(encstream , inpchunk(inpchunk'First)'Address , Integer(inpchunklen) , encblk );
         Write(outfile,encblk,true);
         Put("Encrypted block : ");
         Put_Line( sodium.helpers.bin2hex(encblk.all'Address, encblk.all'Length)) ;
       end loop ;
       declare
          fs : Ada.Streams.Stream_Io.Count := Ada.STreams.Stream_Io.Size(inpfile) ;
          fsblk : Block_Ptr ;
       begin
          Write(encstream,fs'Address,fs'Size/8,fsblk,True);
          Write(outfile,fsblk,true);
       end ;
       Close(encstream);
       Ada.Streams.Stream_Io.Close(inpfile);
       Ada.Streams.Stream_Io.Close(outfile);
   end Encrypt ;



    procedure Decrypt( filein : string ; fileout : string ; pwd : string ) is
       hdr : Block_Ptr ;

       inpfile : Ada.Streams.Stream_Io.File_Type ;

      clrstream : Session_Type ;
      endstream : boolean ;
      outfile : Ada.Streams.Stream_Io.File_Type ;
      clrblockin, clrblock : Block_Ptr ;
      procedure Write( blk : Block_Ptr ) is
         blkbytes : Ada.Streams.Stream_Element_Array(1..blk.all'Length) ;
         for blkbytes'Address use blk.all'Address ;
      begin
         Ada.Streams.Stream_Io.Write( outfile , blkbytes );
      end Write ;

    begin
       Ada.Streams.Stream_Io.Open(inpfile,Ada.Streams.Stream_Io.In_File,filein) ;
       Ada.Streams.Stream_Io.Create(outfile,Ada.Streams.Stream_Io.Out_File,fileout) ;
       Read(inpfile,hdr, Short_Integer(crypto_secretstream_xchacha20poly1305_headerbytes)) ;
       Put("Decrypt Header : ");
       Put_Line(sodium.helpers.bin2hex(hdr.all'Address, hdr.all'Length)) ;
       Create(clrstream,pwd,HeaderPtr_Type(hdr),Inp_Stream) ;
       loop
         Read(inpfile, clrblockin );
         Put("Decrypting block : ");
         Put_Line( sodium.helpers.bin2hex(clrblockin.all'Address, clrblockin.all'Length)) ;
         Read( clrstream,  clrblockin.all'Address , clrblockin.all'Length , clrblock , endstream ) ;
         if endstream
         then
            exit ;
         end if ;
         Write(clrblock) ;
       end loop ;
       Close(clrstream) ;
       Ada.Streams.Stream_Io.Close(inpfile) ;
       Ada.Streams.Stream_Io.Close(outfile) ;
    end Decrypt;

    procedure Show is
        procedure Comment is
        begin
           Put("-- ");
        end Comment ;
    begin
        Comment ; Put("Package: "); Put_Line(GNAT.Source_info.Enclosing_Entity);
        Comment ; Field("Keybytes: ",18); Put(Integer(crypto_secretstream_xchacha20poly1305_keybytes)); NL ;
        Comment ; Field("Statebytes:," , 18); Put(Integer(crypto_secretstream_xchacha20poly1305_statebytes));  NL ;
        Comment ; Field("Headerbytes: " , 18 ); Put(Integer(crypto_secretstream_xchacha20poly1305_headerbytes)); NL ;
        Comment ; Field("Abytes : " , 18); Put(Integer(crypto_secretstream_xchacha20poly1305_abytes)); NL ;
    end Show ;

    procedure Show( key : KeyPtr_Type ) is
    begin
        Put("Key : ");
        Put_Line( sodium.helpers.bin2base64(key.all'Address,size_t(key.all'Length))) ;
    end Show ;

    procedure Show( header : HeaderPtr_Type ) is
    begin
        Put("Header : ");
        Put_Line(sodium.helpers.bin2base64(header.all'Address,size_t(header.all'Length))) ;
    end Show ;

end sodium.sks ;
