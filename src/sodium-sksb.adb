with Interfaces.C ; use Interfaces.C ;
with sodium.random ;

package body sodium.sksb is

    function crypto_secretbox_keybytes return size_t  -- ../include/sodium/crypto_secretbox.h:18
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretbox_keybytes";

   function crypto_secretbox_noncebytes return size_t  -- ../include/sodium/crypto_secretbox.h:22
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretbox_noncebytes";

   function crypto_secretbox_macbytes return size_t  -- ../include/sodium/crypto_secretbox.h:26
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretbox_macbytes";

      procedure crypto_secretbox_keygen (k : Address)  -- ../include/sodium/crypto_secretbox.h:65
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_secretbox_keygen";
   
function crypto_secretbox_easy
     (c : Address;
      m : Address;
      mlen : unsigned_long_long;
      n : Address;
      k : Address) return int  -- ../include/sodium/crypto_secretbox.h:37
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretbox_easy";

   function crypto_secretbox_open_easy
     (m : Address;
      c : Address;
      clen : unsigned_long_long;
      n : Address;
      k : Address) return int  -- ../include/sodium/crypto_secretbox.h:42
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_secretbox_open_easy";
   
   
   function Create return KeyBytes_Ptr is
      result : KeyBytes_Ptr ;
   begin
      result := KeyBytes_Ptr(sodium.Create( crypto_secretbox_keybytes)) ;
      crypto_secretbox_keygen( result.all'Address ) ;
      return result ;
   end Create ;
   
   function Create( random : boolean := true )  return Nonce_Ptr is
      result : Nonce_Ptr ;
   begin
      result := Nonce_Ptr(sodium.Create(crypto_secretbox_noncebytes)) ; 
      if random
      then
         sodium.random.buf(result.all'Address,crypto_secretbox_noncebytes) ;
      end if ;
      return result ;
   end Create ;
   

   function AuthEncrypt( m : Address ; mlen : integer ;
                         n : Nonce_Ptr ; k : KeyBytes_Ptr ) return AuthEncText_Ptr is
      result : AuthEncText_Ptr ;
      status : int ;
   begin
      result := AuthEncText_Ptr(sodium.Create( crypto_secretbox_macbytes + size_t(mlen) )) ;
      status := crypto_secretbox_easy(result.all'Address ,
                                      m , unsigned_long_long(mlen) ,
                                      n.all'Address , k.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_secretbox_easy" ;
      end if ;
      return result ;
   end AuthEncrypt ;
   
   function AuthDecrypt( c : Address ; clen : integer ;
                         n : Nonce_Ptr ; k : KeyBytes_Ptr ) return ClearText_Ptr is
      result : ClearText_Ptr ;
      status : int ;
   begin
      result := ClearText_Ptr(sodium.Create( size_t(clen) - crypto_secretbox_macbytes )) ;
      status := crypto_secretbox_open_easy( result.all'Address ,
                                            c , unsigned_long_long(clen) ,
                                            n.all'Address , k.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_secretbox_open_easy" ;
      end if ;
      return result ;
   end AuthDecrypt ;
   function crypto_secretbox_detached
     (c : Address;
      mac : Address;
      m : Address;
      mlen : unsigned_long_long;
      n : Address;
      k : Address) return int  -- ../include/sodium/crypto_secretbox.h:48
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_secretbox_detached";

   function crypto_secretbox_open_detached
     (m : Address;
      c : Address;
      mac : Address;
      clen : unsigned_long_long;
      n : Address;
      k : Address) return int  -- ../include/sodium/crypto_secretbox.h:56
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_secretbox_open_detached";
   
   procedure AuthEncrypt( c : out EncText_Ptr ;
                          mac : out MAC_Ptr ;
                          m : Address ; mlen : integer ;
                          n : Nonce_Ptr ; k : KeyBytes_Ptr ) is
      status : int ;
   begin
      c := EncText_Ptr(sodium.Create(size_t(mlen) )) ;
      mac := MAC_Ptr(sodium.Create( crypto_secretbox_macbytes )) ;
      status := crypto_secretbox_detached (c.all'Address , mac.all'Address , 
                                      m , unsigned_long_long(mlen) ,
                                      n.all'Address , k.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_secretbox_detached" ;
      end if ;
   end AuthEncrypt ;
   
   procedure AuthDecrypt( m : out ClearText_Ptr ;
                         c : Address ; 
                         mac : MAC_Ptr ;
                         clen : integer ;
                          n : Nonce_Ptr ; k : KeyBytes_Ptr ) is
      status : int ;
   begin
      m := ClearText_Ptr(sodium.Create( size_t(clen) )) ;
      status := crypto_secretbox_open_detached( m.all'Address ,
                                                c , mac.all'Address ,
                                                unsigned_long_long(clen) ,
                                                n.all'Address , k.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_secretbox_open_detached" ;
      end if ;
   end AuthDecrypt ;
   
end sodium.sksb;
