-------------------------------------------------------------------------------
-- Secret Key Cryptography
-- Authenticated encryption
-- https://doc.libsodium.org/secret-key_cryptography/secretbox
-------------------------------------------------------------------------------
with System ; use System ;
package sodium.sksb is

   type KeyBytes_Ptr is new Block_Ptr ;
   type Nonce_Ptr is new Block_Ptr ;
   type AuthEncText_Ptr is new Block_Ptr ;
   type EncText_Ptr is new Block_Ptr ;
   type ClearText_Ptr is new Block_Ptr ;
   type MAC_Ptr is new Block_Ptr ;
   
   function Create return KeyBytes_Ptr;
   function Create( random : boolean := true )  return Nonce_Ptr ;
   
   -- Combined Mode
   -- The text and tag are together in the authenticated encrypted block
   function AuthEncrypt( m : Address ; mlen : integer ;
                         n : Nonce_Ptr ; k : KeyBytes_Ptr ) return AuthEncText_Ptr ;
   function AuthDecrypt( c : Address ; clen : integer ;
                         n : Nonce_Ptr ; k : KeyBytes_Ptr ) return ClearText_Ptr ;
   
   -- Detached Mode
   -- The text and tag are kept in separate blocks
   
   procedure AuthEncrypt( c : out EncText_Ptr ;
                          mac : out MAC_Ptr ;
                          m : Address ; mlen : integer ;
                         n : Nonce_Ptr ; k : KeyBytes_Ptr ) ;
   procedure AuthDecrypt( m : out ClearText_Ptr ;
                         c : Address ; 
                         mac : MAC_Ptr ;
                         clen : integer ;
                         n : Nonce_Ptr ; k : KeyBytes_Ptr ) ;

   
end sodium.sksb;
