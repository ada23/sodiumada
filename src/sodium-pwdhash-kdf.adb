with System; use System ;
with Interfaces ; use Interfaces ;
with Interfaces.C ; use Interfaces.C ;
with Ada.Strings.Fixed ;

package body sodium.pwdhash.kdf is

   function crypto_kdf_bytes_min return size_t  -- ../include/sodium/crypto_kdf.h:19
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_bytes_min";

   function crypto_kdf_bytes_max return size_t  -- ../include/sodium/crypto_kdf.h:23
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_bytes_max";

   function crypto_kdf_contextbytes return size_t  -- ../include/sodium/crypto_kdf.h:27
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_contextbytes";

   function crypto_kdf_keybytes return size_t  -- ../include/sodium/crypto_kdf.h:31
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_keybytes";


   function crypto_kdf_derive_from_key
     (subkey : Address ;
      subkey_len : size_t;
      subkey_id : Unsigned_64;
      ctx : Address;
      key : Address ) return int  -- ../include/sodium/crypto_kdf.h:39
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_derive_from_key";

   procedure crypto_kdf_keygen (k : Address)  -- ../include/sodium/crypto_kdf.h:46
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_keygen";

   function Create( generate : boolean := false ) return MasterKeyPtr_Type is
      result : MasterKeyPtr_Type ;
   begin
      result := MasterKeyPtr_Type(sodium.Create(crypto_kdf_keybytes)) ; 
      crypto_kdf_keygen(result.all'Address) ;
      return result ;
   end Create;
   

   function DeriveFromKey
     (subkey_id : Unsigned_64; ctx : String ; key : MasterKeyPtr_Type) return SubKeyPtr_Type is
      result : SubKeyPtr_Type ;
      ctxstr : String ( 1.. 1+integer(crypto_kdf_contextbytes) ) ;
      status : int ;
   begin
      result := SubKeyPtr_Type( sodium.Create(crypto_kdf_bytes_max) ) ;
      Ada.Strings.Fixed.Move(ctx,ctxstr);
      ctxstr(ctxstr'Length) := ASCII.Nul ; 
     status := crypto_kdf_derive_from_key( result.all'Address , result.all'Length , subkey_id , ctxstr'Address , key.all'Address ) ;
      if status  /= 0
      then
         raise Program_Error with "crypto_kdf_derive_from_key" ;
      end if ;
      return result ;
   end DeriveFromKey ;
   procedure DeriveFromKey
     ( subkey : Address ;
       subkey_len : integer ;
       ctx : String ; key : MasterKeyPtr_Type ;subkey_id : Unsigned_64) is
      ctxstr : String ( 1.. 1+integer(crypto_kdf_contextbytes) ) ;
      status : int ;
   begin
      Ada.Strings.Fixed.Move(ctx,ctxstr);
      ctxstr(ctxstr'Length) := ASCII.Nul ;        
      status := crypto_kdf_derive_from_key( subkey , size_t(subkey_len) , subkey_id , ctxstr'Address , key.all'Address ) ;
      if status  /= 0
      then
         raise Program_Error with "crypto_kdf_derive_from_key" ;
      end if ;
   end DeriveFromKey ;
   
end sodium.pwdhash.kdf;
