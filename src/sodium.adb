with Interfaces.C ; use Interfaces.C ;
with Interfaces.C.strings ; use Interfaces.C.strings ;
with Ada.Strings.Fixed ;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Unchecked_Deallocation ;
with Ada.Streams.Stream_Io ;

with sodium.helpers ;

package body sodium is

   function sodium_init return int  -- ../include/sodium/core.h:12
   with Import => True,
        Convention => C,
        External_Name => "sodium_init";

   function Initialize return boolean is
      status : Int := 0 ;
   begin
      status := sodium_init ;
      if status >= 0
      then
         return true ;
      else
         return false ;
      end if ;
   end Initialize ;

   function sodium_version_string return Interfaces.C.Strings.chars_ptr  -- ../include/sodium/version.h:18
   with Import => True,
        Convention => C,
        External_Name => "sodium_version_string";
   function Version return String is
   begin
      return Value(sodium_version_string) ;
   end Version ;

   function sodium_library_version_major return int  -- ../include/sodium/version.h:21
   with Import => True,
        Convention => C,
        External_Name => "sodium_library_version_major";

   function sodium_library_version_minor return int  -- ../include/sodium/version.h:24
   with Import => True,
        Convention => C,
     External_Name => "sodium_library_version_minor";
   function Major return integer is
   begin
      return Integer(sodium_library_version_major) ;
   end Major ;

   function Minor return integer is
   begin
      return Integer(sodium_library_version_minor) ;
   end Minor ;

   function Value( b64str : String ) return Block_Ptr is
      result : Block_Ptr ;
   begin
      result := sodium.helpers.base642bin(b64str);
      return result ;
   end Value ;


   function Image( key : Block_Ptr ) return String is
   begin
      return sodium.helpers.bin2base64( key.all'address , key'length ) ;
   end Image ;
   function Create( length : size_t ;
                    init : Interfaces.Unsigned_8 := 16#89# ) return Block_Ptr is
      result : Block_Ptr ;
   begin
      result := new Block_Type( 1 .. integer(length) ) ;
      result.all := (others => init) ;
      return result ;
   end Create ;
   procedure Free is new Ada.Unchecked_Deallocation( Block_Type , Block_Ptr) ;
   procedure Destroy(block : in out Block_Ptr) is
   begin
      Free(block);
   end Destroy;


end sodium ;
