-- Sealed boxes
-- https://doc.libsodium.org/public-key_cryptography/sealed_boxes

with System ;
with sodium.crypto ; use sodium.crypto ;

--with sodium_crypto_box_curve25519xsalsa20poly1305_h ; use sodium_crypto_box_curve25519xsalsa20poly1305_h ;
     
package sodium.pksb is

   type Public_KeyPtr is new Block_Ptr ;
   type Secret_KeyPtr is new Block_Ptr ;
   
   procedure Generate( pk : out Public_KeyPTr ;
                       sk : out Secret_KeyPtr ) ;
   
   function Seal( ctp : System.Address ;
                  ctl : Integer ;
                  pk : Public_KeyPtr ) return EncryptedBlock_Ptr ;
   
   function UnSeal( etp : System.Address ;
                    etl : Integer ;
                    pk : Public_KeyPtr ;
                    sk : Secret_KeyPtr ) return ClearText_Ptr ;
   
end sodium.pksb ;
