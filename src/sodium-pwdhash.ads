-------------------------------------------------------------------------------
-- Password hashing
-- https://doc.libsodium.org/password_hashing
-------------------------------------------------------------------------------

package sodium.pwdhash is

   type SaltPtr_type is new Block_Ptr ;
   type HashedPwdPtr_type is new Block_Ptr ;
   type HashedPwdStr_type is new Block_Ptr ;
   type KeyPtr_Type is new Block_Ptr ;
   
   type usage is
     (
      INTERACTIVE , 
      MODERATE , 
      SENSITIVE ) ;
   
   function Create (random : boolean ) return SaltPtr_Type ; 
   function DeriveKey( pwd : String ;
                       salt : SaltPtr_Type ;
                       us : Usage ;
                       rqkeylen : Integer := 0) return KeyPtr_Type ;
   function DeriveKey( pwd : String ;
                       us : Usage  ) return String ; 
   function Verify( Key : String ;
                    pwd : String ) return boolean ;

   procedure Show ;
   procedure Show( s : SaltPtr_type) ;
   procedure Show( h : HashedPwdPtr_type );
   procedure Show( h : HashedPwdStr_Type );
   procedure Show( k : KeyPtr_Type );

end sodium.pwdhash;
