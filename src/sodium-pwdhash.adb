with System; use System;
with Interfaces.C ; use Interfaces.C ;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with GNAT.Source_Info ;

with sodium.random ;
with sodium.helpers ;
package body sodium.pwdhash is

   function crypto_pwhash_alg_default return int  -- ../include/sodium/crypto_pwhash.h:27
     with Import => True, 
        Convention => C, 
     External_Name => "crypto_pwhash_alg_default";
   
   function crypto_pwhash_bytes_min return size_t  -- ../include/sodium/crypto_pwhash.h:31
     with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_bytes_min";

   function crypto_pwhash_bytes_max return size_t  -- ../include/sodium/crypto_pwhash.h:35
     with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_bytes_max";

   function crypto_pwhash_passwd_min return size_t  -- ../include/sodium/crypto_pwhash.h:39
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_passwd_min";

   function crypto_pwhash_passwd_max return size_t  -- ../include/sodium/crypto_pwhash.h:43
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_passwd_max";

   function crypto_pwhash_saltbytes return size_t  -- ../include/sodium/crypto_pwhash.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_saltbytes";

   function crypto_pwhash_strbytes return size_t  -- ../include/sodium/crypto_pwhash.h:51
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_strbytes";

   function crypto_pwhash_opslimit_min return size_t  -- ../include/sodium/crypto_pwhash.h:59
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_opslimit_min";

   function crypto_pwhash_opslimit_max return size_t  -- ../include/sodium/crypto_pwhash.h:63
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_opslimit_max";

   function crypto_pwhash_memlimit_min return size_t  -- ../include/sodium/crypto_pwhash.h:67
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_memlimit_min";

   function crypto_pwhash_memlimit_max return size_t  -- ../include/sodium/crypto_pwhash.h:71
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_memlimit_max";

   function crypto_pwhash_opslimit_interactive return size_t  -- ../include/sodium/crypto_pwhash.h:75
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_opslimit_interactive";

   function crypto_pwhash_memlimit_interactive return size_t  -- ../include/sodium/crypto_pwhash.h:79
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_memlimit_interactive";

   function crypto_pwhash_opslimit_moderate return size_t  -- ../include/sodium/crypto_pwhash.h:83
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_opslimit_moderate";

   function crypto_pwhash_memlimit_moderate return size_t  -- ../include/sodium/crypto_pwhash.h:87
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_memlimit_moderate";

   function crypto_pwhash_opslimit_sensitive return size_t  -- ../include/sodium/crypto_pwhash.h:91
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_opslimit_sensitive";

   function crypto_pwhash_memlimit_sensitive return size_t  -- ../include/sodium/crypto_pwhash.h:95
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_memlimit_sensitive";

   function crypto_pwhash
     (c_out : Address ;
      outlen : unsigned_long_long;
      passwd : Address ;
      passwdlen : unsigned_long_long;
      salt : Address ;
      opslimit : unsigned_long_long;
      memlimit : size_t;
      alg : int) return int  -- ../include/sodium/crypto_pwhash.h:104
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash";

   function crypto_pwhash_str
     (c_out : Address ;
      passwd : Address ;
      passwdlen : unsigned_long_long;
      opslimit : unsigned_long_long;
      memlimit : size_t) return int  -- ../include/sodium/crypto_pwhash.h:116
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_str";

   function crypto_pwhash_str_verify
     (str : Address ;
      passwd : Address ;
      passwdlen : unsigned_long_long) return int  -- ../include/sodium/crypto_pwhash.h:128
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_pwhash_str_verify";

   function crypto_pwhash_str_needs_rehash
     (str : Address ;
      opslimit : unsigned_long_long;
      memlimit : size_t) return int  -- ../include/sodium/crypto_pwhash.h:134
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_pwhash_str_needs_rehash";
   
   function Create (random : boolean ) return SaltPtr_Type is
      result : SaltPTr_Type ;
   begin
      --result := new Block_Type(1..integer(crypto_pwhash_saltbytes)) ;
      result := SaltPtr_Type(sodium.Create(crypto_pwhash_saltbytes) ) ;
      if random
      then
         sodium.random.buf(result.all'Address,result.all'Length) ;
      else
         result.all := (others => 0) ;
      end if ;
      
      return result ;
   end Create ;
   
   function DeriveKey( pwd : String ;
                       salt : SaltPtr_Type ;
                       us : Usage ;
                       rqkeylen : Integer := 0) return KeyPtr_Type is
      result : KeyPtr_Type ;
      status : int ;
      opslimit : unsigned_long_long;
      memlimit : size_t ;
      hb : size_t := crypto_pwhash_bytes_min ;
   begin
      if rqkeylen > 0
      then
         hb := size_t(rqkeylen) ;
      end if ;
      
      --result := new Block_Type(1..integer(hb)) ;
      result := KeyPtr_Type(sodium.Create(hb) ) ;
      case us is
         when INTERACTIVE =>
            opslimit := unsigned_long_long(crypto_pwhash_opslimit_interactive) ;
            memlimit := size_t(crypto_pwhash_memlimit_interactive) ;
         when MODERATE =>
            opslimit := unsigned_long_long(crypto_pwhash_opslimit_moderate) ;
            memlimit := size_t(crypto_pwhash_memlimit_moderate) ;
         when SENSITIVE =>
            opslimit := unsigned_long_long(crypto_pwhash_opslimit_sensitive) ;
            memlimit := size_t(crypto_pwhash_memlimit_sensitive) ;
      end case ;
      
      status := crypto_pwhash(result.all'Address , result.all'Length ,
                              pwd'Address , pwd'Length ,
                              salt.all'Address ,
                              opslimit ,
                              memlimit ,
                              int(crypto_pwhash_alg_default) ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_pwhash" ;
      end if ;
      return result ;
   end DeriveKey ;
   function DeriveKey( pwd : String ;
                       us : Usage ) return String is
      result : char_array(1.. crypto_pwhash_strbytes ) ;
      status : int ;
      opslimit : unsigned_long_long;
      memlimit : size_t ;
      
   begin

      case us is
         when INTERACTIVE =>
            opslimit := unsigned_long_long(crypto_pwhash_opslimit_interactive) ;
            memlimit := size_t(crypto_pwhash_memlimit_interactive) ;
         when MODERATE =>
            opslimit := unsigned_long_long(crypto_pwhash_opslimit_moderate) ;
            memlimit := size_t(crypto_pwhash_memlimit_moderate) ;
         when SENSITIVE =>
            opslimit := unsigned_long_long(crypto_pwhash_opslimit_sensitive) ;
            memlimit := size_t(crypto_pwhash_memlimit_sensitive) ;
      end case ;
      
      status := crypto_pwhash_str(result'Address ,
                              pwd'Address , pwd'Length ,
                              opslimit ,
                              memlimit  ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_pwhash_str" ;
      end if ;
      return To_Ada(result) ;
   end DeriveKey ;
   function Verify( Key : String ;
                    pwd : String ) return boolean is
      status : int ;
      keystr : char_array := To_C(key) ;
   begin
      status := crypto_pwhash_str_verify( keystr'Address , pwd'Address , pwd'Length ) ;
      if status = 0
      then
         return true ;
      end if ;
      return false ;
   end Verify ;

   procedure NL is
   begin
      Ada.Text_Io.New_Line ;
   end NL ;

   package size_t_text_io is new Ada.Text_Io.Modular_Io( size_t );
   use size_t_text_io ;

   procedure Show is
      procedure Comment is
      begin
        Put("-- ");
      end Comment ;
   begin
      Comment; Put("Package      : "); Put(GNAT.Source_info.Enclosing_Entity); NL ;
      Comment; Put("Salt         : "); Put(crypto_pwhash_saltbytes); NL;
      Comment; Put("Key          : "); Put(crypto_pwhash_bytes_min) ; Put( " .. " ); Put(crypto_pwhash_bytes_max); NL ;
      Comment; Put("HashedPwdStr :");  Put(crypto_pwhash_passwd_min) ; Put(" .. "); Put(crypto_pwhash_passwd_max); NL;
   end Show ;

   procedure Show( s : SaltPtr_type) is
   begin
      sodium.helpers.ShowB64(Block_Ptr(s));
   end Show ;

   procedure Show( h : HashedPwdPtr_type ) is
   begin
      sodium.helpers.ShowB64(Block_Ptr(h));
   end Show ;
   procedure Show( h : HashedPwdStr_type ) is
   begin
      sodium.helpers.ShowB64 (Block_Ptr(h));
   end Show ;
   procedure Show( k : KeyPtr_Type ) is
   begin
      sodium.helpers.ShowB64 (Block_Ptr(k)) ;
   end Show ;

end sodium.pwdhash;
