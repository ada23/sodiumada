
-- Authenticated encryption
-- Reference: https://doc.libsodium.org/public-key_cryptography/authenticated_encryption

with System ; use System ;
with Interfaces ; use Interfaces ;

package sodium.pkc is

   type Public_KeyPtr is new Block_Ptr ;
   type Secret_KeyPtr is new Block_Ptr ;
   type Nonce_Ptr is new Block_Ptr ;
   type Seed_Ptr is new Block_Ptr ;
   type MAC_Ptr is new Block_Ptr ;
   type Shared_KeyPtr is new Block_Ptr ;
   
   function Nonce return Nonce_Ptr ;
   function Seed( init : Unsigned_8 := 16#63# ) return Seed_Ptr; 
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr );
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr ; seed : Seed_Ptr );
   function Public( sk : Secret_KeyPtr ) return Public_KeyPtr ;
   
   function Encrypt( m : Address ; mlen : Integer ;
                     n : Nonce_Ptr ;
                     rpk : Public_KeyPtr ;
                     ssk : Secret_KeyPtr ) return Block_Ptr ;
   
   procedure Encrypt( c : out Block_Ptr;
                      mac : out MAC_Ptr ;
                      m : Address ; mlen : Integer ;
                      n : Nonce_Ptr ;
                      rpk : Public_KeyPtr ;
                      ssk : Secret_KeyPtr
                     ) ;
   
   function Decrypt( c : Address ; clen : Integer ;
                     n : Nonce_Ptr ;
                     spk : Public_KeyPtr ;
                     rsk : Secret_KeyPtr ) return Block_Ptr ;
   
   procedure Decrypt( m : out Block_Ptr ;
                      c : Address ; clen : Integer ;
                      mac : MAC_Ptr ;
                      n : Nonce_Ptr ;
                      spk : Public_KeyPtr ;
                      rsk : Secret_KeyPtr ) ;

   function PreCalc( pk : Public_KeyPtr ; sk : Secret_KeyPtr ) return Shared_KeyPtr ;
   function Encrypt( m : Address ; mlen : Integer ;
                     n : Nonce_Ptr ;
                     shk : Shared_KeyPtr ) return Block_Ptr ;
   
   function Decrypt( c : Address ; clen : Integer ;
                     n : Nonce_Ptr ;
                     shk : Shared_KeyPtr ) return Block_Ptr ;
   
   procedure Encrypt( c : out Block_Ptr ;
                      mac : out MAC_Ptr ;
                     m : Address ; mlen : Integer ;
                     n : Nonce_Ptr ;
                     shk : Shared_KeyPtr ) ;
   
   function Decrypt( c : Address ; 
                     mac : MAC_Ptr ;
                     clen : Integer ;
                     n : Nonce_Ptr ;
                     shk : Shared_KeyPtr ) return Block_Ptr ;
end sodium.pkc;
