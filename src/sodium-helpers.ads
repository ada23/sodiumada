with System; use System;
with Interfaces.C; use Interfaces.C;
with Ada.Streams.Stream_Io; 
package sodium.helpers is
 
  procedure memzero (pnt : Address; len : size_t)  -- ../include/sodium/utils.h:22
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_memzero";

   procedure stackzero (len : size_t)  -- ../include/sodium/utils.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_stackzero";

   function memcmp
     (b1_u : Address;
      b2_u : Address;
      len : size_t) return int  -- ../include/sodium/utils.h:34
   with Import => True, 
        Convention => C, 
     External_Name => "sodium_memcmp";

   function compare
     (b1_u : Address ;
      b2_u : Address  ;
      len : size_t) return int  -- ../include/sodium/utils.h:44
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_compare";

   function is_zero (n : Address ; 
                     nlen : size_t) return int  -- ../include/sodium/utils.h:48
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_is_zero";

   function bin2hex(ptr : Address ; len : integer ) return string ;
   function hex2bin(hex : string ) return Block_Ptr ;
   
   BASE64_VARIANT_ORIGINAL : constant := 1;  --  ../include/sodium/utils.h:71
   BASE64_VARIANT_ORIGINAL_NO_PADDING : constant := 3;  --  ../include/sodium/utils.h:72
   BASE64_VARIANT_URLSAFE : constant := 5;  --  ../include/sodium/utils.h:73
   BASE64_VARIANT_URLSAFE_NO_PADDING : constant := 7;  --  ../include/sodium/utils.h:74

   function bin2base64( bin : Address ; bin_len : size_t ;
                        variant : integer := BASE64_VARIANT_ORIGINAL ) 
                       return String ;
   function base642bin( b64 : string ;
                        variant : integer := BASE64_VARIANT_ORIGINAL )
                       return Block_Ptr ;
   
   -- Long Integer
   -- For eg increment keys per message
   
   function sodium_compare
     (b1_u : Address;
      b2_u : Address;
      len : size_t) return int  -- ../include/sodium/utils.h:44
     with Import => True, 
     Convention => C, 
     External_Name => "sodium_compare";

   function sodium_is_zero (n : Address; nlen : size_t) return int  -- ../include/sodium/utils.h:48
     with Import => True, 
     Convention => C, 
     External_Name => "sodium_is_zero";

   procedure sodium_increment (n : Address; nlen : size_t)  -- ../include/sodium/utils.h:51
     with Import => True, 
     Convention => C, 
     External_Name => "sodium_increment";

   procedure sodium_add
     (a : Address;
      b : Address;
      len : size_t)  -- ../include/sodium/utils.h:54
     with Import => True, 
     Convention => C, 
     External_Name => "sodium_add";

   procedure sodium_sub
     (a : Address;
      b : Address;
      len : size_t)  -- ../include/sodium/utils.h:57
     with Import => True, 
     Convention => C, 
     External_Name => "sodium_sub";

  procedure ShowHex( b : Block_Ptr ) ;
  procedure ShowB64( b : Block_Ptr ) ; 
  
   procedure Set( block : Block_Ptr ; value : String ; padding : Character := ASCII.BS ) ;
   function Create(value : String) return Block_Ptr ;
   function Create(value : Ada.Streams.Stream_Element_Array) return Block_Ptr ;
  procedure Write( str : in out Ada.Streams.Stream_Io.File_Type ; b : Block_Ptr );
  procedure Read ( str : in out Ada.Streams.Stream_Io.File_Type ; len : Integer ; b : out Block_Ptr) ;
end sodium.helpers;
