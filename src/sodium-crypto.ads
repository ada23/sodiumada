with Interfaces ;

package sodium.crypto is
   type Key_Type is array (natural range <>) of Interfaces.Unsigned_8 ;
   type EncryptedBlock_Type is array (natural range <>) of Interfaces.Unsigned_8 ;
   type EncryptedBlock_Ptr is access all EncryptedBlock_Type ;
   type ClearText_Type is array (natural range <>) of Interfaces.Unsigned_8 ;
   type ClearText_Ptr is access all ClearText_Type ;
end sodium.crypto;
