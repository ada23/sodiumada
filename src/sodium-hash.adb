with Ada.Text_Io; use Ada.Text_Io;
with Ada.Streams.Stream_IO ;
with sodium.helpers ; use sodium.helpers ;
with sodium.memory ; use sodium.memory ;

package body sodium.hash is
   function crypto_generichash_bytes return size_t  -- ../include/sodium/crypto_generichash.h:26
   with Import => True,
        Convention => C,
        External_Name => "crypto_generichash_bytes";
   function crypto_generichash_statebytes return size_t  -- ../include/sodium/crypto_generichash.h:51
   with Import => True,
        Convention => C,
     External_Name => "crypto_generichash_statebytes";

   function generichash
     (c_out : Address ;
      outlen : size_t;
      c_in : Address ;
      inlen : unsigned_long_long;
      key : Address  ;
      keylen : size_t ) return int  -- ../include/sodium/crypto_generichash.h:54
   with Import => True,
        Convention => C,
     External_Name => "crypto_generichash";

   function Hash( c_in : Address ;
                  inlen : integer ;
                  Key : Address := Null_Address ;
                  keylen : integer := 0 ) return Hash_Ptr is
      result : Hash_Ptr ;
      status : int ;
   begin
      result := Hash_Ptr(sodium.Create(crypto_generichash_statebytes));
      status := generichash( result.all'Address ,
                             result.all'Length ,
                             c_in ,
                             unsigned_long_long(inlen) ,
                             key ,
                             size_t(keylen) ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_generichash" ;
      end if ;
      return result;
   end Hash ;

   function Hash( c_in : Address ;
                  inlen : integer ;
                  Key : Address := Null_Address ;
                  keylen : integer := 0 ) return String is
      sig : Hash_Ptr := Hash( c_in , inlen , key , keylen ) ;
   begin
      return bin2hex(sig.all'address,sig'length) ;
   end Hash ;

   function Hash( c_in : Address ;
                  inlen : integer ;
                  Key : Key_Ptr ) return String is
   begin
      return Hash( c_in , inlen , Key.all'Address , Key.all'Length ) ;
   end Hash ;

   function Create return State_Ptr is
      result : State_Ptr ;
   begin
      result := State_Ptr(sodium.memory.malloc(crypto_generichash_statebytes));
      InitState(result) ;
      return result ;
   end Create ;

   function Create (key : Key_Ptr) return State_Ptr is
      result : State_Ptr ;
   begin
      result := State_Ptr(sodium.memory.malloc(crypto_generichash_statebytes));
      InitState(result, key) ;
      return result ;
   end Create ;

   function generichash_init
     (state : Address ;
      key : Address ;
      keylen : size_t;
      outlen : size_t) return int  -- ../include/sodium/crypto_generichash.h:60
   with Import => True,
        Convention => C,
   External_Name => "crypto_generichash_init";

   procedure InitState( state : State_Ptr ; key : Key_Ptr := null )  is
      status : int ;
      hashlen : size_t := crypto_generichash_bytes ;
   begin
      if key /= null
      then
         status := generichash_init( Address(state) ,
                                     key.all'Address ,
                                     size_t(key.all'Length) ,
                                     hashlen ) ;
      else
         status := generichash_init( Address(state) ,
                                     Null_Address ,
                                     0 ,
                                     hashlen ) ;
      end if ;

      if status /= 0
      then
         raise Program_Error with "generichash_init" ;
      end if ;
   end InitState ;
   function generichash_update
     (state : Address ;
      c_in : Address ;
      inlen : unsigned_long_long) return int  -- ../include/sodium/crypto_generichash.h:66
   with Import => True,
        Convention => C,
     External_Name => "crypto_generichash_update";
   procedure Update( state : State_Ptr ;
                     c_in : Address ;
                     inlen : integer ) is
      status : int ;
   begin
      status := generichash_update( Address(state) ,
                                    c_in ,
                                    unsigned_long_long( inlen ));
      if status /= 0
      then
         raise Program_Error ;
      end if ;
   end Update ;

   function crypto_generichash_final
     (state : Address ;
      c_out : Address ;
      outlen : size_t) return int  -- ../include/sodium/crypto_generichash.h:72
   with Import => True,
        Convention => C,
     External_Name => "crypto_generichash_final";
   function Final( state : State_Ptr ) return Hash_Ptr is
      result : Hash_Ptr;
      status : int ;
   begin
      result := new Block_Type( 1..integer(crypto_generichash_bytes) );
      status := crypto_generichash_final( Address(state) ,
                                          result.all'Address ,
                                          result.All'Length ) ;
      if status /= 0
      then
         raise Program_Error ;
      end if ;
      return result ;
   end Final ;

   function Final( state : State_Ptr ) return String is
      hash : Hash_Ptr := Final( state ) ;
   begin
      return sodium.helpers.bin2hex( hash.all'Address , hash.all'Length ) ;
   end Final ;

   function Digest( filename : string ; key : Key_Ptr := null) return String is
      use Ada.Streams ;
      f : ada.Streams.Stream_IO.File_Type ;
      buffer : ada.Streams.Stream_Element_Array(1..1024) ;
      bufbytes : ada.Streams.Stream_Element_Count ;
      bytes : ada.Streams.Stream_Element_Count := 0 ;
      dig : State_Ptr;
   begin
      if key = null
      then
         dig := Create ;
      else
         dig := Create(key) ;
      end if ;

      ada.Streams.stream_io.Open(f , ada.streams.Stream_IO.In_File , filename ) ;
      while not ada.Streams.Stream_IO.End_Of_File(f)
      loop
         ada.Streams.stream_io.Read(f,buffer,bufbytes) ;
         Update(dig,Buffer'Address, Integer(bufbytes));
         bytes := bytes + bufbytes ;
      end loop ;
      ada.Streams.Stream_IO.Close(f) ;
      return Final(dig) ;
   exception
      when others =>
         put("Unable to generate digest for ");
         put_line(filename) ;
         raise ;
   end digest ;

   function crypto_generichash_keybytes return size_t  -- ../include/sodium/crypto_generichash.h:38
   with Import => True,
        Convention => C,
     External_Name => "crypto_generichash_keybytes";

   procedure crypto_generichash_keygen (k : Address)  -- ../include/sodium/crypto_generichash.h:77
   with Import => True,
        Convention => C,
     External_Name => "crypto_generichash_keygen";

   function Create return Key_Ptr is
      result : Key_Ptr ;
   begin
      result := Key_Ptr(sodium.Create(crypto_generichash_keybytes));
      crypto_generichash_keygen(result.all'Address) ;
      return result ;
   end Create ;

    function crypto_shorthash_bytes return size_t  -- ../include/sodium/crypto_shorthash.h:18
   with Import => True,
        Convention => C,
        External_Name => "crypto_shorthash_bytes";

   function crypto_shorthash_keybytes return size_t  -- ../include/sodium/crypto_shorthash.h:22
   with Import => True,
        Convention => C,
     External_Name => "crypto_shorthash_keybytes";

   function crypto_shorthash
     (c_out : Address ;
      c_in : Address ;
      inlen : unsigned_long_long;
      k : Address ) return int  -- ../include/sodium/crypto_shorthash.h:29
   with Import => True,
        Convention => C,
        External_Name => "crypto_shorthash";

   procedure crypto_shorthash_keygen (k : Address )  -- ../include/sodium/crypto_shorthash.h:34
   with Import => True,
        Convention => C,
     External_Name => "crypto_shorthash_keygen";

   function ShortHash( c_in : Address ;
                       inlen : unsigned_long_long;
                       k : ShortHashKey_Ptr ) return ShortHash_Type is
      result : aliased ShortHash_Type ;
      status : int ;
   begin
      status := crypto_shorthash (result'Address , c_in , inlen , k.all'Address) ;
      if status /= 0
      then
         raise Program_Error with "crypto_shorthash" ;
      end if ;
      return result ;
   end ShortHash ;

   function ShortHashKeyGen(random : boolean := true ) return ShortHashKey_Ptr is
      result : ShortHashKey_Ptr ;
   begin
      result := ShortHashKey_Ptr(sodium.Create(crypto_shorthash_keybytes)) ;
      if random
      then
         crypto_shorthash_keygen(result.all'Address) ;
      else
         result.all := (others => 16#63#) ;
      end if ;

      return result ;
   end ShortHashKeyGen ;

end sodium.hash ;
