
-- Public-key signatures
-- https://doc.libsodium.org/public-key_cryptography/public-key_signatures

with System; use System ;
with Interfaces; use Interfaces ;

package sodium.pks is

   type Public_KeyPtr is new Block_Ptr ;
   type Secret_KeyPtr is new Block_Ptr ;
   type Signature_Ptr is new Block_Ptr ;
   type SignedMsg_Ptr is new Block_Ptr ;
   type Seed_Ptr is new Block_Ptr ;
   
   function Create( init : Unsigned_8 := 16#86# ) return Seed_Ptr; 
   
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr );
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr ; seed : Seed_Ptr );
   
   function Sign( m : Address ; mlen : integer ;
                  sk : Secret_KeyPtr ) return SignedMsg_Ptr ;
   function Open( sm : SignedMsg_Ptr ; pk : Public_KeyPtr ) return Block_Ptr ;
   function Open( sm : Address ; smlen : integer ; pk : Public_KeyPtr ) return Block_Ptr ;
   
   ----------------------------------------------------------------------------  
   -- Detached Variants
   ----------------------------------------------------------------------------
   function Sign( m : Address ; mlen : integer ;
                  sk : Secret_KeyPtr ) return Signature_Ptr ;
   function Verify( sm : Signature_Ptr ;
                    m : SignedMsg_Ptr ;
                    pk : Public_KeyPtr ) return boolean ;
   function Verify( sm : Address ;
                    m : Address ;
                    mlen : integer ;
                    pk : Public_KeyPtr ) return boolean ;
   
   ----------------------------------------------------------------------------
   -- Multipart Variant
   ----------------------------------------------------------------------------
   type Sign_State_Ptr is new Block_Ptr ;
   function Create return Sign_State_Ptr ;
   procedure Update( state : in out Sign_State_Ptr ;
                     m : Address ; mlen : Integer ) ;
   procedure Final( state : in out Sign_State_Ptr ;
                    sk : Secret_KeyPtr ;
                    sig : out Signature_Ptr ) ;
   procedure FinalVerify( state : in out Sign_State_Ptr ;
                          sig : Signature_Ptr ;
                          pk : Public_KeyPtr ;
                          verified : out boolean ) ;
   procedure FinalVerify( state : in out Sign_State_Ptr ;
                          sig : Address ;
                          pk : Public_KeyPtr ;
                          verified : out boolean ) ;

   ----------------------------------------------------------------------------
   -- Convenience functions for files
   ----------------------------------------------------------------------------
   function Signature( fn : String ; sk : Secret_KeyPtr ) return Signature_Ptr ;
   procedure Signature( fn : String ; sk : Secret_KeyPtr; sigfile : string ) ;
   function Verify( fn : string ; sig : Signature_Ptr ; pk : Public_KeyPtr ) return boolean ;
   function Verify( fn : string ; sigfile : String ; pk : Public_KeyPtr ) return boolean ;

end sodium.pks;
