pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sys_upthread_upthread_types_h;

package sys_upthread_upthread_mutexattr_t_h is

   subtype pthread_mutexattr_t is sys_upthread_upthread_types_h.uu_darwin_pthread_mutexattr_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_pthread/_pthread_mutexattr_t.h:31

end sys_upthread_upthread_mutexattr_t_h;
