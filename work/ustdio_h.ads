pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sys_utypes_h;
with System;
with Interfaces.C.Strings;

package ustdio_h is

   subtype fpos_t is sys_utypes_h.uu_darwin_off_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:81

   type uu_sbuf is record
      u_base : access unsigned_char;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:93
      u_size : aliased int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:94
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:92

   type uu_sFILEX is null record;   -- incomplete struct

   type uu_sFILE_array1622 is array (0 .. 2) of aliased unsigned_char;
   type uu_sFILE_array1623 is array (0 .. 0) of aliased unsigned_char;
   type uu_sFILE is record
      u_p : access unsigned_char;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:127
      u_r : aliased int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:128
      u_w : aliased int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:129
      u_flags : aliased short;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:130
      u_file : aliased short;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:131
      u_bf : aliased uu_sbuf;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:132
      u_lbfsize : aliased int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:133
      u_cookie : System.Address;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:136
      u_close : access function (arg1 : System.Address) return int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:137
      u_read : access function
           (arg1 : System.Address;
            arg2 : Interfaces.C.Strings.chars_ptr;
            arg3 : int) return int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:138
      u_seek : access function
           (arg1 : System.Address;
            arg2 : fpos_t;
            arg3 : int) return fpos_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:139
      u_write : access function
           (arg1 : System.Address;
            arg2 : Interfaces.C.Strings.chars_ptr;
            arg3 : int) return int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:140
      u_ub : aliased uu_sbuf;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:143
      u_extra : access uu_sFILEX;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:144
      u_ur : aliased int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:145
      u_ubuf : aliased uu_sFILE_array1622;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:148
      u_nbuf : aliased uu_sFILE_array1623;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:149
      u_lb : aliased uu_sbuf;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:152
      u_blksize : aliased int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:155
      u_offset : aliased fpos_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:156
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:126

   subtype FILE is uu_sFILE;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_stdio.h:157

end ustdio_h;
