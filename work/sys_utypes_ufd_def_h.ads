pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with i386_utypes_h;

package sys_utypes_ufd_def_h is

   type fd_set_array1766 is array (0 .. 31) of aliased i386_utypes_h.uu_int32_t;
   type fd_set is record
      fds_bits : aliased fd_set_array1766;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_fd_def.h:51
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_fd_def.h:50

   --  skipped func __darwin_check_fd_set_overflow

   --  skipped func __darwin_check_fd_set

   --  skipped func __darwin_fd_isset

   --  skipped func __darwin_fd_set

   --  skipped func __darwin_fd_clr

end sys_utypes_ufd_def_h;
