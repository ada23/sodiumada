pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with stddef_h;

package sodium_crypto_scalarmult_ed25519_h is

   crypto_scalarmult_ed25519_BYTES : constant := 32;  --  /Users/rajasrinivasan/include/sodium/crypto_scalarmult_ed25519.h:13

   crypto_scalarmult_ed25519_SCALARBYTES : constant := 32;  --  /Users/rajasrinivasan/include/sodium/crypto_scalarmult_ed25519.h:17

   function crypto_scalarmult_ed25519_bytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_scalarmult_ed25519.h:15
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_ed25519_bytes";

   function crypto_scalarmult_ed25519_scalarbytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_scalarmult_ed25519.h:19
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_ed25519_scalarbytes";

   function crypto_scalarmult_ed25519
     (q : access unsigned_char;
      n : access unsigned_char;
      p : access unsigned_char) return int  -- /Users/rajasrinivasan/include/sodium/crypto_scalarmult_ed25519.h:30
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_ed25519";

   function crypto_scalarmult_ed25519_noclamp
     (q : access unsigned_char;
      n : access unsigned_char;
      p : access unsigned_char) return int  -- /Users/rajasrinivasan/include/sodium/crypto_scalarmult_ed25519.h:35
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_ed25519_noclamp";

   function crypto_scalarmult_ed25519_base (q : access unsigned_char; n : access unsigned_char) return int  -- /Users/rajasrinivasan/include/sodium/crypto_scalarmult_ed25519.h:40
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_ed25519_base";

   function crypto_scalarmult_ed25519_base_noclamp (q : access unsigned_char; n : access unsigned_char) return int  -- /Users/rajasrinivasan/include/sodium/crypto_scalarmult_ed25519.h:44
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_ed25519_base_noclamp";

end sodium_crypto_scalarmult_ed25519_h;
