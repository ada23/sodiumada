pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with i386_utypes_h;

package sys_utypes_uin_port_t_h is

   subtype in_port_t is i386_utypes_h.uu_uint16_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_in_port_t.h:31

end sys_utypes_uin_port_t_h;
