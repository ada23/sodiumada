pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with i386_utypes_h;

package sys_utypes_uintptr_t_h is

   subtype intptr_t is i386_utypes_h.uu_darwin_intptr_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_intptr_t.h:32

end sys_utypes_uintptr_t_h;
