pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with mach_i386_ustructs_h;

package i386_umcontext_h is

   type uu_darwin_mcontext32 is record
      uu_es : aliased mach_i386_ustructs_h.uu_darwin_i386_exception_state;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:43
      uu_ss : aliased mach_i386_ustructs_h.uu_darwin_i386_thread_state;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:44
      uu_fs : aliased mach_i386_ustructs_h.uu_darwin_i386_float_state;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:45
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:41

   type uu_darwin_mcontext_avx32 is record
      uu_es : aliased mach_i386_ustructs_h.uu_darwin_i386_exception_state;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:51
      uu_ss : aliased mach_i386_ustructs_h.uu_darwin_i386_thread_state;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:52
      uu_fs : aliased mach_i386_ustructs_h.uu_darwin_i386_avx_state;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:53
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:49

   type uu_darwin_mcontext_avx512_32 is record
      uu_es : aliased mach_i386_ustructs_h.uu_darwin_i386_exception_state;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:60
      uu_ss : aliased mach_i386_ustructs_h.uu_darwin_i386_thread_state;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:61
      uu_fs : aliased mach_i386_ustructs_h.uu_darwin_i386_avx512_state;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:62
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:58

   type uu_darwin_mcontext64 is record
      uu_es : aliased mach_i386_ustructs_h.uu_darwin_x86_exception_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:101
      uu_ss : aliased mach_i386_ustructs_h.uu_darwin_x86_thread_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:102
      uu_fs : aliased mach_i386_ustructs_h.uu_darwin_x86_float_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:103
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:99

   type uu_darwin_mcontext64_full is record
      uu_es : aliased mach_i386_ustructs_h.uu_darwin_x86_exception_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:109
      uu_ss : aliased mach_i386_ustructs_h.uu_darwin_x86_thread_full_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:110
      uu_fs : aliased mach_i386_ustructs_h.uu_darwin_x86_float_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:111
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:107

   type uu_darwin_mcontext_avx64 is record
      uu_es : aliased mach_i386_ustructs_h.uu_darwin_x86_exception_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:117
      uu_ss : aliased mach_i386_ustructs_h.uu_darwin_x86_thread_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:118
      uu_fs : aliased mach_i386_ustructs_h.uu_darwin_x86_avx_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:119
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:115

   type uu_darwin_mcontext_avx64_full is record
      uu_es : aliased mach_i386_ustructs_h.uu_darwin_x86_exception_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:125
      uu_ss : aliased mach_i386_ustructs_h.uu_darwin_x86_thread_full_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:126
      uu_fs : aliased mach_i386_ustructs_h.uu_darwin_x86_avx_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:127
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:123

   type uu_darwin_mcontext_avx512_64 is record
      uu_es : aliased mach_i386_ustructs_h.uu_darwin_x86_exception_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:134
      uu_ss : aliased mach_i386_ustructs_h.uu_darwin_x86_thread_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:135
      uu_fs : aliased mach_i386_ustructs_h.uu_darwin_x86_avx512_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:136
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:132

   type uu_darwin_mcontext_avx512_64_full is record
      uu_es : aliased mach_i386_ustructs_h.uu_darwin_x86_exception_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:142
      uu_ss : aliased mach_i386_ustructs_h.uu_darwin_x86_thread_full_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:143
      uu_fs : aliased mach_i386_ustructs_h.uu_darwin_x86_avx512_state64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:144
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:140

   type mcontext_t is access all uu_darwin_mcontext64;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/i386/_mcontext.h:206

end i386_umcontext_h;
