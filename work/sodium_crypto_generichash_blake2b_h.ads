pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with stddef_h;
with Interfaces.C.Extensions;

package sodium_crypto_generichash_blake2b_h is

   crypto_generichash_blake2b_BYTES_MIN : constant := 16;  --  /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:37

   crypto_generichash_blake2b_BYTES_MAX : constant := 64;  --  /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:41

   crypto_generichash_blake2b_BYTES : constant := 32;  --  /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:45

   crypto_generichash_blake2b_KEYBYTES_MIN : constant := 16;  --  /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:49

   crypto_generichash_blake2b_KEYBYTES_MAX : constant := 64;  --  /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:53

   crypto_generichash_blake2b_KEYBYTES : constant := 32;  --  /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:57

   crypto_generichash_blake2b_SALTBYTES : constant := 16;  --  /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:61

   crypto_generichash_blake2b_PERSONALBYTES : constant := 16;  --  /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:65

   type crypto_generichash_blake2b_state_array1560 is array (0 .. 383) of aliased unsigned_char;
   type crypto_generichash_blake2b_state is record
      opaque : aliased crypto_generichash_blake2b_state_array1560;  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:26
   end record
   with Convention => C_Pass_By_Copy;  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:25

   function crypto_generichash_blake2b_bytes_min return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:39
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_bytes_min";

   function crypto_generichash_blake2b_bytes_max return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:43
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_bytes_max";

   function crypto_generichash_blake2b_bytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_bytes";

   function crypto_generichash_blake2b_keybytes_min return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:51
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_keybytes_min";

   function crypto_generichash_blake2b_keybytes_max return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:55
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_keybytes_max";

   function crypto_generichash_blake2b_keybytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:59
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_keybytes";

   function crypto_generichash_blake2b_saltbytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:63
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_saltbytes";

   function crypto_generichash_blake2b_personalbytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:67
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_personalbytes";

   function crypto_generichash_blake2b_statebytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:70
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_statebytes";

   function crypto_generichash_blake2b
     (c_out : access unsigned_char;
      outlen : stddef_h.size_t;
      c_in : access unsigned_char;
      inlen : Extensions.unsigned_long_long;
      key : access unsigned_char;
      keylen : stddef_h.size_t) return int  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:73
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b";

   function crypto_generichash_blake2b_salt_personal
     (c_out : access unsigned_char;
      outlen : stddef_h.size_t;
      c_in : access unsigned_char;
      inlen : Extensions.unsigned_long_long;
      key : access unsigned_char;
      keylen : stddef_h.size_t;
      salt : access unsigned_char;
      personal : access unsigned_char) return int  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:80
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_salt_personal";

   function crypto_generichash_blake2b_init
     (state : access crypto_generichash_blake2b_state;
      key : access unsigned_char;
      keylen : stddef_h.size_t;
      outlen : stddef_h.size_t) return int  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:90
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_init";

   function crypto_generichash_blake2b_init_salt_personal
     (state : access crypto_generichash_blake2b_state;
      key : access unsigned_char;
      keylen : stddef_h.size_t;
      outlen : stddef_h.size_t;
      salt : access unsigned_char;
      personal : access unsigned_char) return int  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:96
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_init_salt_personal";

   function crypto_generichash_blake2b_update
     (state : access crypto_generichash_blake2b_state;
      c_in : access unsigned_char;
      inlen : Extensions.unsigned_long_long) return int  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:104
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_update";

   function crypto_generichash_blake2b_final
     (state : access crypto_generichash_blake2b_state;
      c_out : access unsigned_char;
      outlen : stddef_h.size_t) return int  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:110
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_final";

   procedure crypto_generichash_blake2b_keygen (k : access unsigned_char)  -- /Users/rajasrinivasan/include/sodium/crypto_generichash_blake2b.h:115
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_generichash_blake2b_keygen";

end sodium_crypto_generichash_blake2b_h;
