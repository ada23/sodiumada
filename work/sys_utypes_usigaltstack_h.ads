pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with System;
with i386_utypes_h;

package sys_utypes_usigaltstack_h is

   type uu_darwin_sigaltstack is record
      ss_sp : System.Address;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_sigaltstack.h:44
      ss_size : aliased i386_utypes_h.uu_darwin_size_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_sigaltstack.h:45
      ss_flags : aliased int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_sigaltstack.h:46
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_sigaltstack.h:42

   subtype stack_t is uu_darwin_sigaltstack;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_sigaltstack.h:48

end sys_utypes_usigaltstack_h;
