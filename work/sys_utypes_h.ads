pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with i386_utypes_h;

package sys_utypes_h is

   subtype uu_darwin_blkcnt_t is i386_utypes_h.uu_int64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:55

   subtype uu_darwin_blksize_t is i386_utypes_h.uu_int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:56

   subtype uu_darwin_dev_t is i386_utypes_h.uu_int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:57

   subtype uu_darwin_fsblkcnt_t is unsigned;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:58

   subtype uu_darwin_fsfilcnt_t is unsigned;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:59

   subtype uu_darwin_gid_t is i386_utypes_h.uu_uint32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:60

   subtype uu_darwin_id_t is i386_utypes_h.uu_uint32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:61

   subtype uu_darwin_ino64_t is i386_utypes_h.uu_uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:62

   subtype uu_darwin_ino_t is uu_darwin_ino64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:64

   subtype uu_darwin_mach_port_name_t is i386_utypes_h.uu_darwin_natural_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:68

   subtype uu_darwin_mach_port_t is uu_darwin_mach_port_name_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:69

   subtype uu_darwin_mode_t is i386_utypes_h.uu_uint16_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:70

   subtype uu_darwin_off_t is i386_utypes_h.uu_int64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:71

   subtype uu_darwin_pid_t is i386_utypes_h.uu_int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:72

   subtype uu_darwin_sigset_t is i386_utypes_h.uu_uint32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:73

   subtype uu_darwin_suseconds_t is i386_utypes_h.uu_int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:74

   subtype uu_darwin_uid_t is i386_utypes_h.uu_uint32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:75

   subtype uu_darwin_useconds_t is i386_utypes_h.uu_uint32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:76

   type uu_darwin_uuid_t is array (0 .. 15) of aliased unsigned_char;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:77

   subtype uu_darwin_uuid_string_t is Interfaces.C.char_array (0 .. 36);  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types.h:78

end sys_utypes_h;
