pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sys_upthread_upthread_types_h;

package sys_upthread_upthread_rwlock_t_h is

   subtype pthread_rwlock_t is sys_upthread_upthread_types_h.uu_darwin_pthread_rwlock_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_pthread/_pthread_rwlock_t.h:31

end sys_upthread_upthread_rwlock_t_h;
