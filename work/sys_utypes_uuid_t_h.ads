pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sys_utypes_h;

package sys_utypes_uuid_t_h is

   subtype uid_t is sys_utypes_h.uu_darwin_uid_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_uid_t.h:31

end sys_utypes_uuid_t_h;
