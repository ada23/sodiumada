pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;

package cpp_11_2_0_bits_std_abs_h is

   function c_abs (uu_i : long) return long  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/bits/std_abs.h:56
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3absl";

   function c_abs (uu_x : Long_Long_Integer) return Long_Long_Integer  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/bits/std_abs.h:61
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3absx";

   function c_abs (uu_x : double) return double  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/bits/std_abs.h:71
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3absd";

   function c_abs (uu_x : float) return float  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/bits/std_abs.h:75
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3absf";

   function c_abs (uu_x : long_double) return long_double  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/bits/std_abs.h:79
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3abse";

   function c_abs (uu_x : Extensions.Signed_128) return Extensions.Signed_128  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/bits/std_abs.h:85
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3absn";

   function c_abs (uu_x : Extensions.Float_128) return Extensions.Float_128  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/bits/std_abs.h:103
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3absg";

end cpp_11_2_0_bits_std_abs_h;
