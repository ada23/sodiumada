pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with System;

package stdarg_h is

   subtype uu_gnuc_va_list is System.Address;  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/lib/gcc/x86_64-apple-darwin19.6.0/11.2.0/include/stdarg.h:40

end stdarg_h;
