pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sys_utypes_h;

package sys_utypes_ufsblkcnt_t_h is

   subtype fsblkcnt_t is sys_utypes_h.uu_darwin_fsblkcnt_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_fsblkcnt_t.h:31

end sys_utypes_ufsblkcnt_t_h;
