pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sys_upthread_upthread_types_h;

package sys_upthread_upthread_key_t_h is

   subtype pthread_key_t is sys_upthread_upthread_types_h.uu_darwin_pthread_key_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_pthread/_pthread_key_t.h:31

end sys_upthread_upthread_key_t_h;
