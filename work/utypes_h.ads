pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with i386_utypes_h;

package utypes_h is

   subtype uu_darwin_nl_item is int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_types.h:40

   subtype uu_darwin_wctrans_t is int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_types.h:41

   subtype uu_darwin_wctype_t is i386_utypes_h.uu_uint32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/_types.h:43

end utypes_h;
