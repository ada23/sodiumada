pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with i386_utypes_h;
with sys_utypes_utimeval_h;
with System;
with utypes_uuint8_t_h;
with utypes_uuint64_t_h;
with utypes_uuint32_t_h;
with sys_utypes_uint32_t_h;
with sys_utypes_uid_t_h;

package sys_resource_h is

   PRIO_PROCESS : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:100
   PRIO_PGRP : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:101
   PRIO_USER : constant := 2;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:102

   PRIO_DARWIN_THREAD : constant := 3;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:105
   PRIO_DARWIN_PROCESS : constant := 4;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:106

   PRIO_MIN : constant := -20;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:112
   PRIO_MAX : constant := 20;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:113

   PRIO_DARWIN_BG : constant := 16#1000#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:120

   PRIO_DARWIN_NONUI : constant := 16#1001#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:126

   RUSAGE_SELF : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:140
   RUSAGE_CHILDREN : constant := -1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:141
   --  unsupported macro: ru_first ru_ixrss
   --  unsupported macro: ru_last ru_nivcsw

   RUSAGE_INFO_V0 : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:186
   RUSAGE_INFO_V1 : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:187
   RUSAGE_INFO_V2 : constant := 2;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:188
   RUSAGE_INFO_V3 : constant := 3;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:189
   RUSAGE_INFO_V4 : constant := 4;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:190
   RUSAGE_INFO_V5 : constant := 5;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:191
   --  unsupported macro: RUSAGE_INFO_CURRENT RUSAGE_INFO_V5

   RU_PROC_RUNS_RESLIDE : constant := 16#00000001#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:197
   --  unsupported macro: RLIM_INFINITY (((__uint64_t)1 << 63) - 1)
   --  unsupported macro: RLIM_SAVED_MAX RLIM_INFINITY
   --  unsupported macro: RLIM_SAVED_CUR RLIM_INFINITY

   RLIMIT_CPU : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:390
   RLIMIT_FSIZE : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:391
   RLIMIT_DATA : constant := 2;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:392
   RLIMIT_STACK : constant := 3;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:393
   RLIMIT_CORE : constant := 4;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:394
   RLIMIT_AS : constant := 5;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:395
   --  unsupported macro: RLIMIT_RSS RLIMIT_AS

   RLIMIT_MEMLOCK : constant := 6;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:398
   RLIMIT_NPROC : constant := 7;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:399

   RLIMIT_NOFILE : constant := 8;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:401

   RLIM_NLIMITS : constant := 9;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:403

   RLIMIT_WAKEUPS_MONITOR : constant := 16#1#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:422
   RLIMIT_CPU_USAGE_MONITOR : constant := 16#2#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:423
   RLIMIT_THREAD_CPULIMITS : constant := 16#3#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:424
   RLIMIT_FOOTPRINT_INTERVAL : constant := 16#4#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:425

   WAKEMON_ENABLE : constant := 16#01#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:430
   WAKEMON_DISABLE : constant := 16#02#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:431
   WAKEMON_GET_PARAMS : constant := 16#04#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:432
   WAKEMON_SET_DEFAULTS : constant := 16#08#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:433
   WAKEMON_MAKE_FATAL : constant := 16#10#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:434

   CPUMON_MAKE_FATAL : constant := 16#1000#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:439

   FOOTPRINT_INTERVAL_RESET : constant := 16#1#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:444

   IOPOL_TYPE_DISK : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:454
   IOPOL_TYPE_VFS_ATIME_UPDATES : constant := 2;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:455
   IOPOL_TYPE_VFS_MATERIALIZE_DATALESS_FILES : constant := 3;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:456
   IOPOL_TYPE_VFS_STATFS_NO_DATA_VOLUME : constant := 4;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:457
   IOPOL_TYPE_VFS_TRIGGER_RESOLVE : constant := 5;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:458
   IOPOL_TYPE_VFS_IGNORE_CONTENT_PROTECTION : constant := 6;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:459
   IOPOL_TYPE_VFS_IGNORE_PERMISSIONS : constant := 7;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:460
   IOPOL_TYPE_VFS_SKIP_MTIME_UPDATE : constant := 8;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:461
   IOPOL_TYPE_VFS_ALLOW_LOW_SPACE_WRITES : constant := 9;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:462

   IOPOL_SCOPE_PROCESS : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:465
   IOPOL_SCOPE_THREAD : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:466
   IOPOL_SCOPE_DARWIN_BG : constant := 2;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:467

   IOPOL_DEFAULT : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:470
   IOPOL_IMPORTANT : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:471
   IOPOL_PASSIVE : constant := 2;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:472
   IOPOL_THROTTLE : constant := 3;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:473
   IOPOL_UTILITY : constant := 4;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:474
   IOPOL_STANDARD : constant := 5;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:475
   --  unsupported macro: IOPOL_APPLICATION IOPOL_STANDARD
   --  unsupported macro: IOPOL_NORMAL IOPOL_IMPORTANT

   IOPOL_ATIME_UPDATES_DEFAULT : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:482
   IOPOL_ATIME_UPDATES_OFF : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:483

   IOPOL_MATERIALIZE_DATALESS_FILES_DEFAULT : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:485
   IOPOL_MATERIALIZE_DATALESS_FILES_OFF : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:486
   IOPOL_MATERIALIZE_DATALESS_FILES_ON : constant := 2;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:487

   IOPOL_VFS_STATFS_NO_DATA_VOLUME_DEFAULT : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:489
   IOPOL_VFS_STATFS_FORCE_NO_DATA_VOLUME : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:490

   IOPOL_VFS_TRIGGER_RESOLVE_DEFAULT : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:492
   IOPOL_VFS_TRIGGER_RESOLVE_OFF : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:493

   IOPOL_VFS_CONTENT_PROTECTION_DEFAULT : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:495
   IOPOL_VFS_CONTENT_PROTECTION_IGNORE : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:496

   IOPOL_VFS_IGNORE_PERMISSIONS_OFF : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:498
   IOPOL_VFS_IGNORE_PERMISSIONS_ON : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:499

   IOPOL_VFS_SKIP_MTIME_UPDATE_OFF : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:501
   IOPOL_VFS_SKIP_MTIME_UPDATE_ON : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:502

   IOPOL_VFS_ALLOW_LOW_SPACE_WRITES_OFF : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:504
   IOPOL_VFS_ALLOW_LOW_SPACE_WRITES_ON : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:505

   subtype rlim_t is i386_utypes_h.uu_uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:89

   type rusage is record
      ru_utime : aliased sys_utypes_utimeval_h.timeval;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:153
      ru_stime : aliased sys_utypes_utimeval_h.timeval;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:154
      ru_maxrss : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:163
      ru_ixrss : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:165
      ru_idrss : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:166
      ru_isrss : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:167
      ru_minflt : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:168
      ru_majflt : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:169
      ru_nswap : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:170
      ru_inblock : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:171
      ru_oublock : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:172
      ru_msgsnd : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:173
      ru_msgrcv : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:174
      ru_nsignals : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:175
      ru_nvcsw : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:176
      ru_nivcsw : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:177
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:152

   type rusage_info_t is new System.Address;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:199

   type rusage_info_v0_array1234 is array (0 .. 15) of aliased utypes_uuint8_t_h.uint8_t;
   type rusage_info_v0 is record
      ri_uuid : aliased rusage_info_v0_array1234;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:202
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:203
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:204
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:205
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:206
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:207
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:208
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:209
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:210
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:211
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:212
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:201

   type rusage_info_v1_array1234 is array (0 .. 15) of aliased utypes_uuint8_t_h.uint8_t;
   type rusage_info_v1 is record
      ri_uuid : aliased rusage_info_v1_array1234;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:216
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:217
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:218
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:219
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:220
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:221
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:222
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:223
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:224
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:225
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:226
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:227
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:228
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:229
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:230
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:231
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:232
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:215

   type rusage_info_v2_array1234 is array (0 .. 15) of aliased utypes_uuint8_t_h.uint8_t;
   type rusage_info_v2 is record
      ri_uuid : aliased rusage_info_v2_array1234;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:236
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:237
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:238
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:239
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:240
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:241
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:242
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:243
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:244
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:245
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:246
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:247
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:248
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:249
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:250
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:251
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:252
      ri_diskio_bytesread : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:253
      ri_diskio_byteswritten : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:254
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:235

   type rusage_info_v3_array1234 is array (0 .. 15) of aliased utypes_uuint8_t_h.uint8_t;
   type rusage_info_v3 is record
      ri_uuid : aliased rusage_info_v3_array1234;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:258
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:259
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:260
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:261
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:262
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:263
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:264
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:265
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:266
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:267
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:268
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:269
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:270
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:271
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:272
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:273
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:274
      ri_diskio_bytesread : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:275
      ri_diskio_byteswritten : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:276
      ri_cpu_time_qos_default : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:277
      ri_cpu_time_qos_maintenance : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:278
      ri_cpu_time_qos_background : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:279
      ri_cpu_time_qos_utility : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:280
      ri_cpu_time_qos_legacy : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:281
      ri_cpu_time_qos_user_initiated : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:282
      ri_cpu_time_qos_user_interactive : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:283
      ri_billed_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:284
      ri_serviced_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:285
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:257

   type rusage_info_v4_array1234 is array (0 .. 15) of aliased utypes_uuint8_t_h.uint8_t;
   type rusage_info_v4 is record
      ri_uuid : aliased rusage_info_v4_array1234;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:289
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:290
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:291
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:292
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:293
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:294
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:295
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:296
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:297
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:298
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:299
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:300
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:301
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:302
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:303
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:304
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:305
      ri_diskio_bytesread : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:306
      ri_diskio_byteswritten : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:307
      ri_cpu_time_qos_default : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:308
      ri_cpu_time_qos_maintenance : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:309
      ri_cpu_time_qos_background : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:310
      ri_cpu_time_qos_utility : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:311
      ri_cpu_time_qos_legacy : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:312
      ri_cpu_time_qos_user_initiated : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:313
      ri_cpu_time_qos_user_interactive : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:314
      ri_billed_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:315
      ri_serviced_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:316
      ri_logical_writes : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:317
      ri_lifetime_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:318
      ri_instructions : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:319
      ri_cycles : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:320
      ri_billed_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:321
      ri_serviced_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:322
      ri_interval_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:323
      ri_runnable_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:324
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:288

   type rusage_info_v5_array1234 is array (0 .. 15) of aliased utypes_uuint8_t_h.uint8_t;
   type rusage_info_v5 is record
      ri_uuid : aliased rusage_info_v5_array1234;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:328
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:329
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:330
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:331
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:332
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:333
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:334
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:335
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:336
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:337
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:338
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:339
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:340
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:341
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:342
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:343
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:344
      ri_diskio_bytesread : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:345
      ri_diskio_byteswritten : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:346
      ri_cpu_time_qos_default : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:347
      ri_cpu_time_qos_maintenance : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:348
      ri_cpu_time_qos_background : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:349
      ri_cpu_time_qos_utility : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:350
      ri_cpu_time_qos_legacy : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:351
      ri_cpu_time_qos_user_initiated : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:352
      ri_cpu_time_qos_user_interactive : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:353
      ri_billed_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:354
      ri_serviced_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:355
      ri_logical_writes : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:356
      ri_lifetime_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:357
      ri_instructions : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:358
      ri_cycles : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:359
      ri_billed_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:360
      ri_serviced_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:361
      ri_interval_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:362
      ri_runnable_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:363
      ri_flags : aliased utypes_uuint64_t_h.uint64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:364
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:327

   subtype rusage_info_current is rusage_info_v5;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:367

   type rlimit is record
      rlim_cur : aliased rlim_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:412
      rlim_max : aliased rlim_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:413
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:411

   type proc_rlimit_control_wakeupmon is record
      wm_flags : aliased utypes_uuint32_t_h.uint32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:447
      wm_rate : aliased sys_utypes_uint32_t_h.int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:448
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:446

   function getpriority (arg1 : int; arg2 : sys_utypes_uid_t_h.id_t) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:511
   with Import => True, 
        Convention => C, 
        External_Name => "getpriority";

   function getiopolicy_np (arg1 : int; arg2 : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:513
   with Import => True, 
        Convention => C, 
        External_Name => "getiopolicy_np";

   function getrlimit (arg1 : int; arg2 : access rlimit) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:515
   with Import => True, 
        Convention => C, 
        External_Name => "_getrlimit";

   function getrusage (arg1 : int; arg2 : access rusage) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:516
   with Import => True, 
        Convention => C, 
        External_Name => "getrusage";

   function setpriority
     (arg1 : int;
      arg2 : sys_utypes_uid_t_h.id_t;
      arg3 : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:517
   with Import => True, 
        Convention => C, 
        External_Name => "setpriority";

   function setiopolicy_np
     (arg1 : int;
      arg2 : int;
      arg3 : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:519
   with Import => True, 
        Convention => C, 
        External_Name => "setiopolicy_np";

   function setrlimit (arg1 : int; arg2 : access constant rlimit) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/resource.h:521
   with Import => True, 
        Convention => C, 
        External_Name => "_setrlimit";

end sys_resource_h;
