pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with i386_utypes_h;

package sys_utypes_ursize_t_h is

   subtype rsize_t is i386_utypes_h.uu_darwin_size_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_rsize_t.h:31

end sys_utypes_ursize_t_h;
