pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sys_utypes_h;

package sys_utypes_uino64_t_h is

   subtype ino64_t is sys_utypes_h.uu_darwin_ino64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_ino64_t.h:31

end sys_utypes_uino64_t_h;
