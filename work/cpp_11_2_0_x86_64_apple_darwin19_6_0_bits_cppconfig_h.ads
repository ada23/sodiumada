pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with System;

package cpp_11_2_0_x86_64_apple_darwin19_6_0_bits_cppconfig_h is

   LT_OBJDIR : aliased constant String := ".libs/" & ASCII.NUL;  --  /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/x86_64-apple-darwin19.6.0/bits/c++config.h:1497

   STDC_HEADERS : constant := 1;  --  /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/x86_64-apple-darwin19.6.0/bits/c++config.h:1539

   subtype size_t is unsigned_long;  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/x86_64-apple-darwin19.6.0/bits/c++config.h:280

   subtype ptrdiff_t is long;  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/x86_64-apple-darwin19.6.0/bits/c++config.h:281

   subtype nullptr_t is System.Address;  -- /Users/rajasrinivasan/tools/bin/gnat_native_11.2.3_f008a8a7/include/c++/11.2.0/x86_64-apple-darwin19.6.0/bits/c++config.h:284

end cpp_11_2_0_x86_64_apple_darwin19_6_0_bits_cppconfig_h;
