pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sodium_randombytes_h;

package sodium_randombytes_sysrandom_h is

   randombytes_sysrandom_implementation : aliased sodium_randombytes_h.randombytes_implementation  -- /Users/rajasrinivasan/include/sodium/randombytes_sysrandom.h:13
   with Import => True, 
        Convention => C, 
        External_Name => "randombytes_sysrandom_implementation";

end sodium_randombytes_sysrandom_h;
