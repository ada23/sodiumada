pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with utypes_uuint64_t_h;
with utypes_uuint8_t_h;
with stddef_h;
with Interfaces.C.Extensions;

package sodium_crypto_hash_sha512_h is

   crypto_hash_sha512_BYTES : constant := 64;  --  /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:33

   type crypto_hash_sha512_state_array1446 is array (0 .. 7) of aliased utypes_uuint64_t_h.uint64_t;
   type crypto_hash_sha512_state_array1448 is array (0 .. 1) of aliased utypes_uuint64_t_h.uint64_t;
   type crypto_hash_sha512_state_array1450 is array (0 .. 127) of aliased utypes_uuint8_t_h.uint8_t;
   type crypto_hash_sha512_state is record
      state : aliased crypto_hash_sha512_state_array1446;  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:25
      count : aliased crypto_hash_sha512_state_array1448;  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:26
      buf : aliased crypto_hash_sha512_state_array1450;  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:27
   end record
   with Convention => C_Pass_By_Copy;  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:24

   function crypto_hash_sha512_statebytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:31
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha512_statebytes";

   function crypto_hash_sha512_bytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:35
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha512_bytes";

   function crypto_hash_sha512
     (c_out : access unsigned_char;
      c_in : access unsigned_char;
      inlen : Extensions.unsigned_long_long) return int  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:38
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha512";

   function crypto_hash_sha512_init (state : access crypto_hash_sha512_state) return int  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:42
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha512_init";

   function crypto_hash_sha512_update
     (state : access crypto_hash_sha512_state;
      c_in : access unsigned_char;
      inlen : Extensions.unsigned_long_long) return int  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:46
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha512_update";

   function crypto_hash_sha512_final (state : access crypto_hash_sha512_state; c_out : access unsigned_char) return int  -- /Users/rajasrinivasan/include/sodium/crypto_hash_sha512.h:52
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha512_final";

end sodium_crypto_hash_sha512_h;
