pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with stddef_h;
with Interfaces.C.Extensions;
with Interfaces.C.Strings;

package sodium_crypto_hash_h is

   --  unsupported macro: crypto_hash_BYTES crypto_hash_sha512_BYTES
   crypto_hash_PRIMITIVE : aliased constant String := "sha512" & ASCII.NUL;  --  /Users/rajasrinivasan/include/sodium/crypto_hash.h:31

   function crypto_hash_bytes return stddef_h.size_t  -- /Users/rajasrinivasan/include/sodium/crypto_hash.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_bytes";

   function crypto_hash
     (c_out : access unsigned_char;
      c_in : access unsigned_char;
      inlen : Extensions.unsigned_long_long) return int  -- /Users/rajasrinivasan/include/sodium/crypto_hash.h:28
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash";

   function crypto_hash_primitive return Interfaces.C.Strings.chars_ptr  -- /Users/rajasrinivasan/include/sodium/crypto_hash.h:33
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_primitive";

end sodium_crypto_hash_h;
