pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with arm_utypes_h;

package sys_utypes_ukey_t_h is

   subtype key_t is arm_utypes_h.uu_int32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_key_t.h:31

end sys_utypes_ukey_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
