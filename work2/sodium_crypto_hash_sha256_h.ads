pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with utypes_uuint32_t_h;
with utypes_uuint8_t_h;
with utypes_uuint64_t_h;
with stddef_h;
with Interfaces.C.Extensions;

package sodium_crypto_hash_sha256_h is

   crypto_hash_sha256_BYTES : constant := 32;  --  /usr/local/include/sodium/crypto_hash_sha256.h:33

   type anon_array2054 is array (0 .. 7) of aliased utypes_uuint32_t_h.uint32_t;
   type anon_array2056 is array (0 .. 63) of aliased utypes_uuint8_t_h.uint8_t;
   type crypto_hash_sha256_state is record
      state : aliased anon_array2054;  -- /usr/local/include/sodium/crypto_hash_sha256.h:25
      count : aliased utypes_uuint64_t_h.uint64_t;  -- /usr/local/include/sodium/crypto_hash_sha256.h:26
      buf : aliased anon_array2056;  -- /usr/local/include/sodium/crypto_hash_sha256.h:27
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/include/sodium/crypto_hash_sha256.h:24

   function crypto_hash_sha256_statebytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_hash_sha256.h:31
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha256_statebytes";

   function crypto_hash_sha256_bytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_hash_sha256.h:35
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha256_bytes";

   function crypto_hash_sha256
     (c_out : access unsigned_char;
      c_in : access unsigned_char;
      inlen : Extensions.unsigned_long_long) return int  -- /usr/local/include/sodium/crypto_hash_sha256.h:38
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha256";

   function crypto_hash_sha256_init (state : access crypto_hash_sha256_state) return int  -- /usr/local/include/sodium/crypto_hash_sha256.h:42
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha256_init";

   function crypto_hash_sha256_update
     (state : access crypto_hash_sha256_state;
      c_in : access unsigned_char;
      inlen : Extensions.unsigned_long_long) return int  -- /usr/local/include/sodium/crypto_hash_sha256.h:46
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha256_update";

   function crypto_hash_sha256_final (state : access crypto_hash_sha256_state; c_out : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_hash_sha256.h:52
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_hash_sha256_final";

end sodium_crypto_hash_sha256_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
