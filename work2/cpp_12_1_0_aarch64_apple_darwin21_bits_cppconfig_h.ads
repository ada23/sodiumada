pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;

package cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h is

   subtype size_t is unsigned_long;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/c++config.h:298

   subtype ptrdiff_t is long;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/c++config.h:299

   subtype nullptr_t is System.Address;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/c++config.h:302

   --  skipped func __terminate

   procedure c_terminate  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/c++config.h:311
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9terminatev";

   --  skipped func __is_constant_evaluated

end cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
