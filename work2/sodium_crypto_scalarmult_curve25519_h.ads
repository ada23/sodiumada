pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with stddef_h;

package sodium_crypto_scalarmult_curve25519_h is

   crypto_scalarmult_curve25519_BYTES : constant := 32;  --  /usr/local/include/sodium/crypto_scalarmult_curve25519.h:12

   crypto_scalarmult_curve25519_SCALARBYTES : constant := 32;  --  /usr/local/include/sodium/crypto_scalarmult_curve25519.h:16

   function crypto_scalarmult_curve25519_bytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_scalarmult_curve25519.h:14
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_curve25519_bytes";

   function crypto_scalarmult_curve25519_scalarbytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_scalarmult_curve25519.h:18
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_curve25519_scalarbytes";

   function crypto_scalarmult_curve25519
     (q : access unsigned_char;
      n : access unsigned_char;
      p : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_scalarmult_curve25519.h:29
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_curve25519";

   function crypto_scalarmult_curve25519_base (q : access unsigned_char; n : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_scalarmult_curve25519.h:34
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_scalarmult_curve25519_base";

end sodium_crypto_scalarmult_curve25519_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
