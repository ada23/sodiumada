pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with System;

package stddef_h is

   --  unsupported macro: NULL __null
   --  arg-macro: procedure offsetof (TYPE, MEMBER)
   --    __builtin_offsetof (TYPE, MEMBER)
   subtype ptrdiff_t is long;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stddef.h:145

   subtype size_t is unsigned_long;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stddef.h:214

   type max_align_t is record
      uu_max_align_ll : aliased Long_Long_Integer;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stddef.h:425
      uu_max_align_ld : aliased long_double;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stddef.h:426
      uu_max_align_f128 : aliased Extensions.Float_128;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stddef.h:432
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stddef.h:434

   subtype nullptr_t is System.Address;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stddef.h:441

end stddef_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
