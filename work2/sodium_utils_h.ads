pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;
with stddef_h;
with Interfaces.C.Strings;

package sodium_utils_h is

   sodium_base64_VARIANT_ORIGINAL : constant := 1;  --  /usr/local/include/sodium/utils.h:71
   sodium_base64_VARIANT_ORIGINAL_NO_PADDING : constant := 3;  --  /usr/local/include/sodium/utils.h:72
   sodium_base64_VARIANT_URLSAFE : constant := 5;  --  /usr/local/include/sodium/utils.h:73
   sodium_base64_VARIANT_URLSAFE_NO_PADDING : constant := 7;  --  /usr/local/include/sodium/utils.h:74
   --  arg-macro: function sodium_base64_ENCODED_LEN (BIN_LEN, VARIANT)
   --    return ((BIN_LEN) / 3) * 4 + ((((BIN_LEN) - ((BIN_LEN) / 3) * 3) or (((BIN_LEN) - ((BIN_LEN) / 3) * 3) >> 1)) and 1) * (4 - (~((((VARIANT) and 2) >> 1) - 1) and (3 - ((BIN_LEN) - ((BIN_LEN) / 3) * 3)))) + 1;

   procedure sodium_memzero (pnt : System.Address; len : stddef_h.size_t)  -- /usr/local/include/sodium/utils.h:22
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_memzero";

   procedure sodium_stackzero (len : stddef_h.size_t)  -- /usr/local/include/sodium/utils.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_stackzero";

   function sodium_memcmp
     (b1_u : System.Address;
      b2_u : System.Address;
      len : stddef_h.size_t) return int  -- /usr/local/include/sodium/utils.h:34
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_memcmp";

   function sodium_compare
     (b1_u : access unsigned_char;
      b2_u : access unsigned_char;
      len : stddef_h.size_t) return int  -- /usr/local/include/sodium/utils.h:44
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_compare";

   function sodium_is_zero (n : access unsigned_char; nlen : stddef_h.size_t) return int  -- /usr/local/include/sodium/utils.h:48
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_is_zero";

   procedure sodium_increment (n : access unsigned_char; nlen : stddef_h.size_t)  -- /usr/local/include/sodium/utils.h:51
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_increment";

   procedure sodium_add
     (a : access unsigned_char;
      b : access unsigned_char;
      len : stddef_h.size_t)  -- /usr/local/include/sodium/utils.h:54
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_add";

   procedure sodium_sub
     (a : access unsigned_char;
      b : access unsigned_char;
      len : stddef_h.size_t)  -- /usr/local/include/sodium/utils.h:57
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_sub";

   function sodium_bin2hex
     (hex : Interfaces.C.Strings.chars_ptr;
      hex_maxlen : stddef_h.size_t;
      bin : access unsigned_char;
      bin_len : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr  -- /usr/local/include/sodium/utils.h:60
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_bin2hex";

   function sodium_hex2bin
     (bin : access unsigned_char;
      bin_maxlen : stddef_h.size_t;
      hex : Interfaces.C.Strings.chars_ptr;
      hex_len : stddef_h.size_t;
      ignore : Interfaces.C.Strings.chars_ptr;
      bin_len : access unsigned_long;
      hex_end : System.Address) return int  -- /usr/local/include/sodium/utils.h:65
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_hex2bin";

   function sodium_base64_encoded_len (bin_len : stddef_h.size_t; variant : int) return stddef_h.size_t  -- /usr/local/include/sodium/utils.h:86
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_base64_encoded_len";

   function sodium_bin2base64
     (b64 : Interfaces.C.Strings.chars_ptr;
      b64_maxlen : stddef_h.size_t;
      bin : access unsigned_char;
      bin_len : stddef_h.size_t;
      variant : int) return Interfaces.C.Strings.chars_ptr  -- /usr/local/include/sodium/utils.h:89
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_bin2base64";

   function sodium_base642bin
     (bin : access unsigned_char;
      bin_maxlen : stddef_h.size_t;
      b64 : Interfaces.C.Strings.chars_ptr;
      b64_len : stddef_h.size_t;
      ignore : Interfaces.C.Strings.chars_ptr;
      bin_len : access unsigned_long;
      b64_end : System.Address;
      variant : int) return int  -- /usr/local/include/sodium/utils.h:94
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_base642bin";

   function sodium_mlock (addr : System.Address; len : stddef_h.size_t) return int  -- /usr/local/include/sodium/utils.h:101
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_mlock";

   function sodium_munlock (addr : System.Address; len : stddef_h.size_t) return int  -- /usr/local/include/sodium/utils.h:105
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_munlock";

   function sodium_malloc (size : stddef_h.size_t) return System.Address  -- /usr/local/include/sodium/utils.h:142
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_malloc";

   function sodium_allocarray (count : stddef_h.size_t; size : stddef_h.size_t) return System.Address  -- /usr/local/include/sodium/utils.h:146
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_allocarray";

   procedure sodium_free (ptr : System.Address)  -- /usr/local/include/sodium/utils.h:150
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_free";

   function sodium_mprotect_noaccess (ptr : System.Address) return int  -- /usr/local/include/sodium/utils.h:153
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_mprotect_noaccess";

   function sodium_mprotect_readonly (ptr : System.Address) return int  -- /usr/local/include/sodium/utils.h:156
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_mprotect_readonly";

   function sodium_mprotect_readwrite (ptr : System.Address) return int  -- /usr/local/include/sodium/utils.h:159
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_mprotect_readwrite";

   function sodium_pad
     (padded_buflen_p : access unsigned_long;
      buf : access unsigned_char;
      unpadded_buflen : stddef_h.size_t;
      blocksize : stddef_h.size_t;
      max_buflen : stddef_h.size_t) return int  -- /usr/local/include/sodium/utils.h:162
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_pad";

   function sodium_unpad
     (unpadded_buflen_p : access unsigned_long;
      buf : access unsigned_char;
      padded_buflen : stddef_h.size_t;
      blocksize : stddef_h.size_t) return int  -- /usr/local/include/sodium/utils.h:167
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_unpad";

   --  skipped func _sodium_alloc_init

end sodium_utils_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
