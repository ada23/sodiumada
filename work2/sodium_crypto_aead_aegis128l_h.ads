pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with stddef_h;
with Interfaces.C.Extensions;

package sodium_crypto_aead_aegis128l_h is

   crypto_aead_aegis128l_KEYBYTES : constant := 16;  --  /usr/local/include/sodium/crypto_aead_aegis128l.h:15

   crypto_aead_aegis128l_NSECBYTES : constant := 0;  --  /usr/local/include/sodium/crypto_aead_aegis128l.h:19

   crypto_aead_aegis128l_NPUBBYTES : constant := 16;  --  /usr/local/include/sodium/crypto_aead_aegis128l.h:23

   crypto_aead_aegis128l_ABYTES : constant := 32;  --  /usr/local/include/sodium/crypto_aead_aegis128l.h:27
   --  unsupported macro: crypto_aead_aegis128l_MESSAGEBYTES_MAX SODIUM_MIN(SODIUM_SIZE_MAX - crypto_aead_aegis128l_ABYTES, (1ULL << 61) - 1)

   function crypto_aead_aegis128l_keybytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:17
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_keybytes";

   function crypto_aead_aegis128l_nsecbytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:21
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_nsecbytes";

   function crypto_aead_aegis128l_npubbytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_npubbytes";

   function crypto_aead_aegis128l_abytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:29
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_abytes";

   function crypto_aead_aegis128l_messagebytes_max return stddef_h.size_t  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:34
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_messagebytes_max";

   function crypto_aead_aegis128l_encrypt
     (c : access unsigned_char;
      clen_p : access Extensions.unsigned_long_long;
      m : access unsigned_char;
      mlen : Extensions.unsigned_long_long;
      ad : access unsigned_char;
      adlen : Extensions.unsigned_long_long;
      nsec : access unsigned_char;
      npub : access unsigned_char;
      k : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:37
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_encrypt";

   function crypto_aead_aegis128l_decrypt
     (m : access unsigned_char;
      mlen_p : access Extensions.unsigned_long_long;
      nsec : access unsigned_char;
      c : access unsigned_char;
      clen : Extensions.unsigned_long_long;
      ad : access unsigned_char;
      adlen : Extensions.unsigned_long_long;
      npub : access unsigned_char;
      k : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:48
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_decrypt";

   function crypto_aead_aegis128l_encrypt_detached
     (c : access unsigned_char;
      mac : access unsigned_char;
      maclen_p : access Extensions.unsigned_long_long;
      m : access unsigned_char;
      mlen : Extensions.unsigned_long_long;
      ad : access unsigned_char;
      adlen : Extensions.unsigned_long_long;
      nsec : access unsigned_char;
      npub : access unsigned_char;
      k : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:60
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_encrypt_detached";

   function crypto_aead_aegis128l_decrypt_detached
     (m : access unsigned_char;
      nsec : access unsigned_char;
      c : access unsigned_char;
      clen : Extensions.unsigned_long_long;
      mac : access unsigned_char;
      ad : access unsigned_char;
      adlen : Extensions.unsigned_long_long;
      npub : access unsigned_char;
      k : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:73
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_decrypt_detached";

   procedure crypto_aead_aegis128l_keygen (k : access unsigned_char)  -- /usr/local/include/sodium/crypto_aead_aegis128l.h:85
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_aead_aegis128l_keygen";

end sodium_crypto_aead_aegis128l_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
