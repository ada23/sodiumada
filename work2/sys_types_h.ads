pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_uu_int64_t_h;
with sys_utypes_uint64_t_h;
with sys_utypes_uint32_t_h;
with sys_utypes_uu_int32_t_h;
with arm_utypes_h;
with sys_utypes_udev_t_h;

package sys_types_h is

   --  unsupported macro: NBBY __DARWIN_NBBY
   --  unsupported macro: NFDBITS __DARWIN_NFDBITS
   --  arg-macro: procedure howmany (x, y)
   --    __DARWIN_howmany(x, y)
   subtype u_long is unsigned_long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:88

   subtype ushort is unsigned_short;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:91

   subtype uint is unsigned;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:92

   subtype u_quad_t is sys_utypes_uu_int64_t_h.u_int64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:95

   subtype quad_t is sys_utypes_uint64_t_h.int64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:96

   type qaddr_t is access all quad_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:97

   subtype daddr_t is sys_utypes_uint32_t_h.int32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:101

   subtype fixpt_t is sys_utypes_uu_int32_t_h.u_int32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:105

   subtype segsz_t is sys_utypes_uint32_t_h.int32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:125

   subtype swblk_t is sys_utypes_uint32_t_h.int32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:126

   function major (u_x : arm_utypes_h.uu_uint32_t) return arm_utypes_h.uu_int32_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:139
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZL5majorj";

   function minor (u_x : arm_utypes_h.uu_uint32_t) return arm_utypes_h.uu_int32_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:145
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZL5minorj";

   function makedev (u_major : arm_utypes_h.uu_uint32_t; u_minor : arm_utypes_h.uu_uint32_t) return sys_utypes_udev_t_h.dev_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:151
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZL7makedevjj";

   subtype fd_mask is arm_utypes_h.uu_int32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/types.h:189

end sys_types_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
