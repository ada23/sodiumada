pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_h;

package sys_utypes_uino_t_h is

   subtype ino_t is sys_utypes_h.uu_darwin_ino_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_ino_t.h:31

end sys_utypes_uino_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
