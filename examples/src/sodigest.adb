with ada.command_line ;
with text_io; use text_io ;
with Interfaces ;

with sodium.hash ;
with sodium.pwdhash ;
with sodium.helpers ;
with sodium.pwdhash.kdf ;

procedure sodigest is
   fn : string := ada.command_line.Argument(1);
   dig : string := sodium.hash.Digest(fn);
   pwdsalt: sodium.pwdhash.SaltPtr_type := sodium.pwdhash.Create(false) ;
   newpwd : sodium.pwdhash.KeyPtr_Type ;
   ini : boolean ;
   kdfkey : sodium.pwdhash.kdf.MasterKeyPtr_Type ;
begin
   ini := sodium.Initialize ;
   put_line(dig);
   declare
      sh : sodium.hash.ShortHash_Type :=
        sodium.hash.ShortHash( fn'address , fn'length , sodium.hash.ShortHashKeyGen );
   begin
      Put( sodium.hash.ShortHash_Type'Image(sh));
      New_Line ;
   end ;
   Put_Line("Using the hash as the password");
   pwdsalt.all := (others => 16#56# );

   newpwd := sodium.pwdhash.DeriveKey(dig,pwdsalt,sodium.pwdhash.INTERACTIVE);
   Put("Key derived ");
   Put_Line(sodium.helpers.bin2hex(newpwd.all'Address,newpwd.all'Length) ) ;
   declare
      newpwd1 : string := sodium.pwdhash.DeriveKey(dig,sodium.pwdhash.INTERACTIVE);
      newpwd2 : string := sodium.pwdhash.DeriveKey(dig,sodium.pwdhash.MODERATE);
      newpwd3 : string := sodium.pwdhash.DeriveKey(dig,sodium.pwdhash.SENSITIVE);
   begin
      
      Put("Interactive "); Put_Line(newpwd1);
      Put("Moderate    "); Put_Line(newpwd2);
      Put("Sensitive   "); Put_Line(newpwd3);
      
      Put_Line("Verification ");
      Put("Interactive "); Put(boolean'image(sodium.pwdhash.Verify(newpwd1,dig) )); New_Line ; 
      Put("Moderate "); Put(boolean'image(sodium.pwdhash.Verify(newpwd2,dig) )); New_Line ; 
      Put("Sensitive "); Put(boolean'image(sodium.pwdhash.Verify(newpwd3,dig) )); New_Line ; 
   end ;
   
   kdfkey := sodium.pwdhash.kdf.Create(true) ;
   Put("KDF masterkey ");
   Put_Line(sodium.helpers.bin2hex(kdfkey.all'Address,kdfkey.all'Length));
      
end sodigest;
