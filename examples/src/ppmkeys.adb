with text_Io; use text_io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_IO ;
--  with keys ; use keys ;
--  with keys.pub ;
--  with keys.pvt ;

with sodium.crypto ; use sodium.crypto;
with sodium.pksb ;


procedure ppmkeys is
   test_message : string := "One flew over cuckoo's nest" ;
   pk : sodium.pksb.Public_KeyPtr ;
   sk : sodium.pksb.Secret_KeyPtr ;
   encmsg : sodium.crypto.EncryptedBlock_Ptr ;
   decmsg : sodium.crypto.ClearText_Ptr ;
begin
   if not sodium.Initialize 
   then
      Put_Line("Failed to initialize sodium library");
      return ;
   end if ;
   sodium.pksb.Generate(pk,sk);
   Put("Original Text length "); Put(test_message'Length); Put_Line(" bytes");
   encmsg := sodium.pksb.Seal(test_message'Address ,
                                    test_message'Length , pk) ;
   Put("Encrypted to : "); Put( encmsg'Length ); Put_Line(" bytes");
   decmsg := sodium.pksb.UnSeal(encmsg.all'Address,
                                      encmsg'Length ,pk,sk) ;
   Put("Decrypted to : "); Put(decmsg'Length); Put_Line(" bytes");
   declare
      decrypted : string(1..decmsg'Length) ;
      for decrypted'address use decmsg.all'Address ;
   begin
      Put("Cleartext result is : "); Put_Line(decrypted) ;
   end ;
end ppmkeys;
