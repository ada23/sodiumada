with Ada.Command_Line; use Ada.Command_Line ;
with sodium.sks ;

procedure soencrypt is
   pwd : String := Argument(1) ;
   inpfile : String := Argument(2) ;
   outfile : String := inpfile & ".enc" ;
begin
   sodium.sks.Encrypt(inpfile,outfile,pwd);
end soencrypt ;
