with Ada.Command_Line ; use Ada.Command_Line ;
with Ada.Text_Io; use Ada.Text_Io ;
with Interfaces; use Interfaces ;

with sodium.version ;
with sodium.helpers ;
with sodium.pks ;

procedure digsign is
   fn : String := Argument(1) ;
   sigfn : String := fn & ".sig" ;
   sb : Unsigned_8 := Unsigned_8'Value(Argument(2)) ;
   seed : sodium.pks.Seed_Ptr := sodium.pks.Create(sb) ;
   sk : sodium.pks.Secret_KeyPtr ;
   pk : sodium.pks.Public_KeyPtr ;
   sig : sodium.pks.Signature_Ptr ;
begin
   Put("Sodium Library Version "); Put( sodium.version.SString ); New_Line;
   sodium.pks.Generate(pk,sk,seed) ;
   Put("Seed : "); Put_Line( sodium.helpers.bin2hex(seed.all'Address,seed.all'Length) ) ;
   Put("Sk   : "); Put_Line( sodium.helpers.bin2hex(sk.all'Address,sk.all'Length) ) ;
   Put("Pk   : "); Put_Line( sodium.helpers.bin2hex(pk.all'Address,pk.all'Length) ) ;
   sodium.pks.Signature(fn,sk,sigfn) ;
   Put_Line("Signature File created");
   Put("Sig Verify : "); Put_Line(boolean'Image(sodium.pks.Verify(fn,sigfn,pk))) ;
   sig := sodium.pks.Signature(fn,sk);
   Put("Signature : ");
   Put_Line(sodium.helpers.bin2hex(sig.all'Address,sig.all'Length));
end digsign;

