with Ada.Command_Line ;
with Text_Io; use Text_Io ;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO ;
--with hex ;
with sodium.crypto ; use sodium.crypto ;
with sodium.pksb ; 

with sodium.helpers ;

procedure ppmkeygen is
   pkgname : string := Ada.Command_Line.Argument(1) ;
   pk : sodium.pksb.Public_KeyPtr ;
   sk : sodium.pksb.Secret_KeyPtr ;
   codefile : File_Type;
begin
   if not sodium.Initialize  
   then
      Put_Line("Failed to initialize sodium library");
      return ;
   end if ;
   sodium.pksb.Generate(pk,sk);
   
   Create(codefile,out_file,pkgname & ".ads" );
   Set_Output(codefile);
   Put("package "); Put(pkgname); Put_Line(" is");
   Put("end "); Put(Pkgname) ; Put_Line(" ;");
   close(codefile) ;
   
   Create(codefile,Out_File,pkgname & "-pvt.ads");
   Set_Output(codefile);
   Put("package "); Put(pkgname); Put_Line(".pvt is");
   --  Put("    key : String := "); Put('"');
   --  Put( hex.Image( sk.all'address , sk'Length ) ) ;
   --  --Put(sodium.functions.As_Hexidecimal(sk));
   --  Put('"');
   --  Put_Line(" ;");
   
   declare
      sk_sodium : string := sodium.helpers.bin2hex( sk.all'address , sk'length ) ;
      skb64_sodium : string := sodium.helpers.bin2base64( sk.all'address , sk'length ) ;
      skrt_sodium : sodium.Block_Ptr := sodium.helpers.base642bin(skb64_sodium) ;
   begin
      Put("    key : String := "); Put('"');
      Put(sk_sodium(1..sk_sodium'length-1));   
      Put('"');
      Put_Line(" ;") ;
      
      Put("    kb64 : String := "); Put('"');
      Put(skb64_sodium(1..skb64_sodium'length-1));   
      Put('"');
      Put_Line(" ;") ;
      
      --  Put("    skrt : String := "); Put('"');
      --  Put( hex.Image( skrt_sodium.all'address , skrt_sodium'Length ) ) ;
      --  Put('"');
      
   end ;     
   --  Put_Line(" ;");
   Put("end "); Put(Pkgname) ; Put_Line(".pvt ;");
   close(codefile);
   
   Create(codefile,Out_File,pkgname & "-pub.ads");
   Set_Output(codefile) ;
   Put("package "); Put(pkgname); Put_Line(".pub is");
   --  Put("    key : String := "); Put('"');
   --  Put( hex.Image( pk.all'Address , pk'length ) ) ;
   --  --Put(sodium.functions.As_Hexidecimal(pk));
   --  Put('"');
   --  Put_Line(" ;");
   declare
      pk_sodium : string := sodium.helpers.bin2hex( pk.all'address , pk'length ) ;
      pkb64_sodium : string := sodium.helpers.bin2base64( pk.all'address , pk'length ) ;      
      pkrt_sodium : sodium.Block_Ptr  := sodium.helpers.base642bin(pkb64_sodium) ;
   begin
      Put("    key : String := "); Put('"');
      Put(pk_sodium(1..pk_sodium'length-1));   
      Put('"');
      Put_Line(" ;") ;
      Put("    kb64 : String := "); Put('"');
      Put(pkb64_sodium(1..pkb64_sodium'length-1));   
      Put('"'); 
      Put_Line(" ;");
      
      --  Put("    pkrt : String := "); Put('"');
      --  Put( hex.Image( pkrt_sodium.all'address , pkrt_sodium'Length ) ) ;
      --  Put('"');
      
   end ; 
   --  Put_Line(" ;");
   Put("end "); Put(Pkgname) ; Put_Line(".pub ;");
   close(codefile) ;
   
end ppmkeygen;
