with sodium.sks ;
with sodium.pwdhash ;

procedure skstest is
   key : sodium.sks.KeyPtr_Type ;
   key2 : sodium.sks.KeyPtr_Type ;
   salt : sodium.pwdhash.SaltPtr_type ;
begin
   sodium.sks.Show ;
   key := sodium.sks.Generate("skstest" , salt );
   sodium.sks.Show(key) ;
   sodium.sks.Save(key,"skstest.key") ;
   sodium.sks.Load("skstest.key",key2) ;
   sodium.sks.Show(key2);
end skstest;