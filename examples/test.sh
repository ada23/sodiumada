#!/bin/bash
bin/soencrypt srini skstest.key
bin/sodecrypt srini skstest.key.enc
diff skstest.key skstest.key.enc.dec
echo Now decrypting with a different password
bin/sodecrypt srinivasan skstest.key.enc
echo Now testing a large binary encrypt/decrypt
bin/soencrypt srinivasan bin/soencrypt
bin/sodecrypt srinivasan bin/soencrypt.enc
echo Testing large binary with different password
bin/sodecrypt srini bin/soencrypt.enc
